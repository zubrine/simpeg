<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends Public_Controller
{
	function __construct(){
		parent::__construct();	
	}
	
	function index()
	{	
		$this->meta->meta_title = "Login pengguna";
		$this->load->view($this->layout_login);	
	}
	
	function main(){
		
		$data['page'] = 'layout_home';
		$this->load->view($this->layout,$data);
	}
	
}