<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pegawai extends Admin_Controller
{
	function __construct(){
		parent::__construct();
		$this->module = strtolower(__CLASS__);	
	}
	
	function data()
	{

		/*$grid_option = array();
		$grid_option['rownumbers'] 		= 'true';
		$grid_option['singleSelect'] 	= 'true';
		$grid_option['pagination'] 		= 'true';
		$grid_option['url'] 			= site_url("pegawai/json_data");
		$grid_option['method'] 			= 'post';
		$option = gridOption($grid_option);
			
		$data['gridname'] = "data_pegawai";
		$data['title'] = "Data pegawai";
		$data['grid_option'] = $option;*/
		
		$this->crud->set_theme('flexigrid');
		$this->crud->set_table('sp_pegawai');
		$this->crud->set_subject('Data pegawai');
		$this->crud->required_fields('title');
		$this->crud->columns('id','nip','nama','jenis_kelamin','gelar_depan','gelar_belakang','alamat');
		//$crud->display_as('title','Judul')->display_as('author','Penulis')->display_as('content','Isi');
		//$crud->set_multilanguage( array('title','content') );
		$this->crud->add_action('More', '', 'demo/action_more','ui-icon-plus');
		$this->crud->set_field_upload('image','assets/uploads/files');
		$output = $this->crud->render();
		
		$data['output'] = $output;
		$data['page'] = "pegawai_data";
		$data['module'] = $this->module;
		$this->load->view($this->layout_content,$data);		
	}
	
	function json_data()
	{
		$pegawai = array();
		
		$start = 0;
		$perpage = 10;
		$total = 100;
		$count = 10;

		if( isset($_GET['page']) && isset($_GET['rows']) && $_GET['page'] > 1 )
		{
			$totalpage = ceil(100/$_GET['rows']);
			$start = $_GET['rows']*$_GET['page'];
			$perpage = $_GET['rows'];
			$count = ($count*$_GET['page'])+$perpage;
		}
		
		$pegawai['total'] = $total;
		$o=0;
		for($i=$start;$i<=$count;$i++)
		{
			
			$pegawai['rows'][$o] = array();
			$pegawai['rows'][$o]['nip'] = '0000000000'.$i;
			$pegawai['rows'][$o]['nama_pegawai'] = "Nama pegawai $i";
			$pegawai['rows'][$o]['unit_kerja'] = "Unit Kerja $i";
			$pegawai['rows'][$o]['jabatan'] = "Jabatan $i";
			$pegawai['rows'][$o]['status'] = "Status $i";
			$o++;
	
		}
		
		$json = json_encode((array)$pegawai);
		echo $json;
		
		//adodb_pr( json_decode('{"total":"1","rows":[{"id":"83289","firstname":"","lastname":"","phone":"","email":""}]}',1));
		//adodb_pr($pegawai);
		
	}
	
	
}