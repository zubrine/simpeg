<?php 
foreach($output->css_files as $file): ?>
	<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>
<?php foreach($output->js_files as $file): ?>
	<script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>
<?php echo $output->output; ?>
<? /*
<table id="<?=$gridname?>" title="<?=$title?>" data-options="<?=$grid_option?>" style="100%" toolbar="#tb">
    <thead>
        <tr>
            <th data-options="field:'nip',width:200">NIP</th>
            <th data-options="field:'nama_pegawai',width:400,align:'left'">Nama pegawai</th>
            <th data-options="field:'unit_kerja',width:200,align:'center'">Unit Kerja</th>
            <th data-options="field:'jabatan',width:100">Jabatan</th>
            <th data-options="field:'status',width:60,align:'center'">Status</th>
        </tr>
    </thead>
</table>
<div id="tb" style="padding:5px;height:auto">
    <div style="margin-bottom:5px">
        <a href="#" id="pegawai-add"></a>
        <a href="#" id="pegawai-edit"></a>
        <a href="#" id="pegawai-delete"></a>
    </div>
    <div>
       
    </div>
</div>

<div class="x-dialog" data-options="resizable:true" buttons="#buttons" modal="true" id="form-pegawai" closed="true" style="height:500px;width:700px;" title="Tambah pegawai">
<form id="form" method="post">
			<input name="id" type="hidden">
			<table class="table table-form" style="width:100%;">
				<tr>
					<td width="30%"><label>NIP</label></td>
					<td width="70%"><input id="nip" type="text" name="nip" class="easyui-validatebox" data-options="required:true"></td>
				</tr>
                <tr>
					<td width="30%"><label>NIP Lama</label></td>
					<td width="70%"><input id="nip_lama" type="text" name="nip_lama" class="easyui-validatebox" data-options="required:true"></td>
				</tr>
				<tr>
					<td width="30%"><label>Nama</label></td>
					<td width="70%"><input id="nama" type="text" name="nama" class="easyui-validatebox" data-options="required:true"></td>
				</tr>
				<tr>
					<td width="30%"><label>Jenis Kelamin</label></td>
					<td width="70%">
                    <select name="jenis_kelamin">
							<option value="m">Laki-laki</option>
							<option value="f">Perempuan</option>
					</select>
                    </td>
				</tr>
				<tr>
					<td width="30%"><label>Gelar depan</label></td>
					<td width="70%"><input id="gelar_depan" type="text" name="gelar_depan"></td>
				</tr>
                <tr>
					<td width="30%"><label>Gelar belakang</label></td>
					<td width="70%"><input id="gelar_belakang" type="text" name="gelar_belakang"></td>
				</tr>
                <tr>
					<td width="30%"><label>Gelar lain</label></td>
					<td width="70%"><input id="gelar_lain" type="text" name="gelar_lain"></td>
				</tr>
				<tr>
					<td width="30%"><label>Tempat lahir</label></td>
					<td width="70%"><input type="text" name="tempat_lahir"  id="tempat_lahir"></td>
				</tr>
                <tr>
					<td width="30%"><label>Tanggal lahir</label></td>
					<td width="70%"><input type="text" name="tanggal_lahir"  id="tanggal_lahir"></td>
				</tr>
				<tr>
					<td width="30%"><label>Address</label></td>
					<td width="70%"><textarea name="address" rows="5"></textarea></td>
				</tr>
                <tr>
					<td width="30%">&nbsp;</td>
					<td width="70%">
                    
                    <div id="buttons">
		<a href="javascript:void(0)" onclick="submitForm()" class="x-linkbutton">Submit</a>
	</div></td>
				</tr>
			</table>
		</form>
</div>

<script type="text/javascript">

$(document).ready(function(){
	$('#<?=$gridname?>').datagrid().datagrid('getPager');
	
	$('#pegawai-add').linkbutton({
		iconCls: 'icon-add',
		text:'Tambah pegawai',
		onClick:function(){
			$('#form-pegawai').dialog('open').dialog('resize');
		}
	});
	
	$('#pegawai-edit').linkbutton({
		iconCls: 'icon-edit',
		text:'Edit pegawai',
		onClick:function(){
			var row = $('#<?=$gridname?>').datagrid('getSelected');
			if(row){
				alert(row.nip);	
			}else{
				alert("Silahkan pilih data");	
			}
		}
	});
	$('#pegawai-delete').linkbutton({
		iconCls: 'icon-delete',
		text:'Hapus pegawai',
		onClick:function(){
			var row = $('#<?=$gridname?>').datagrid('getSelected');
			if(row){
				alert(row.nip);	
			}else{
				alert("Silahkan pilih data");	
			}
		}
	});
	
	/*$('#pegawai-tab').tabs({
		border:false
	});
	
	//$('#tanggal_lahir').datebox(options);
		
});
function submitForm()
	{
		//alert('submited');	
		//$('#form').form('submit');
		/* $('#form').form({
			success:function(data){
				$.messager.alert('Info', data, 'info');
			}
		});
			$('#name').validatebox({
				required: true,
				validType: 'email'
			});
	}

</script>
 */?>