<script type="text/javascript">
$(document).ready(function() {
    $('.click-this').click(function(){
		var thisVal = $(this).val();
		if( $(this).is(':checked') ){
			$('.status-'+thisVal).val('1');
		}else{
			$('.status-'+thisVal).val('0');	
		}
	});
});
</script>
<div id="peaBoxContent">
<?php echo isset($msg) ? $msg : '';?>
<form method="post" action="" name="edit" enctype="multipart/form-data">
<table border="0" cellspacing="0" cellpadding="0" class="formTbl" >
<thead>
    <tr class="title">
        <td id="formTblHead" colspan="3"><strong>Ubah permission</strong></td>
    </tr>
</thead>
<tbody>
<?php
echo checkbox_resources($resources);
?>
</tbody>
<tfoot>
    <tr class="title">
        <td valign="top" colspan="3">
            <input name="change_permission" type="submit" value="&nbsp;SAVE&nbsp;"  default="true" class="pea-button-save" >
        </td>
    </tr>
</tfoot>

</table>
</form>
</div>