<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends Admin_Controller
{
	var $user_table;
	var $user_primary;
	var $role_table;
	var $role_primary;
	var $role_field_name;
	
	function __construct(){
		parent::__construct();	
		$this->user_table = 'sys_users';
		$this->user_primary = 'id';
		$this->role_table = 'sys_roles';
		$this->role_primary = 'id';
		$this->role_field_name = 'name';
		$this->load->model('dx_auth/permissions', 'permissions'); 
	}
	
	function index(){
		$this->data();	
	}
	
	function data()
	{
		check_user( uri_string() );
		$this->system->add_breadcrumb('Data user');
	
		$formRoll = new phpEasyAdmin($this->user_table);
		$formRoll->initRoll("ORDER BY id DESC");
		
		$formRoll->roll->addInput("header","header");
		$formRoll->roll->input->header->setTitle("Data user");
		
		$formRoll->roll->addInput('username','text');
		$formRoll->roll->input->username->setTitle('Username');
		$formRoll->roll->input->username->setPlaintext(true);
		
		$formRoll->roll->addInput('role_id','selecttable');
		$formRoll->roll->input->role_id->setTitle('Group');
		$formRoll->roll->input->role_id->setReferenceTable($this->role_table);
		$formRoll->roll->input->role_id->setReferenceField($this->role_field_name,$this->role_primary);
		$formRoll->roll->input->role_id->setPlaintext(true);
		
		$formRoll->roll->addInput('name','text');
		$formRoll->roll->input->name->setTitle('Nama');
		$formRoll->roll->input->name->setPlaintext(true);
		
		$formRoll->roll->addInput('email','text');
		$formRoll->roll->input->email->setTitle('Email');
		$formRoll->roll->input->email->setPlaintext(true);
		
		#$formRoll->roll->addInput('created','text');
		#$formRoll->roll->input->created->setTitle('Created');
		#$formRoll->roll->input->created->setPlaintext(true);
		
		$formRoll->roll->addInput('last_login','text');
		$formRoll->roll->input->last_login->setTitle('Terakhir login');
		$formRoll->roll->input->last_login->setPlaintext(true);
		
		#$formRoll->roll->addInput('banned','checkbox');
		#$formRoll->roll->input->banned->setTitle('Banned');
		#$formRoll->roll->input->banned->setCaption('Ya');
		
		#$formRoll->roll->addInput('ban_reason','text');
		#$formRoll->roll->input->ban_reason->setTitle('Alasan banned');
		#$formRoll->roll->input->ban_reason->setPlaintext(true);
		
		$formRoll->roll->addInput("id","editlinks");
		$formRoll->roll->input->id->setTitle("Edit");
		$formRoll->roll->input->id->setCaption("Edit");
		$formRoll->roll->input->id->setLinks( site_url('_cpanel/admin/user/edit') );
		
		$formRoll->roll->action();		
		$tabs['Data user'] = $formRoll->roll->getForm();
		
	
		$this->pea->setTable($this->user_table);
		$this->pea->initEdit();
		
		$this->pea->edit->addInput("header","header");
		$this->pea->edit->input->header->setTitle("Tambah user");
		
		$this->pea->edit->addInput('username','text');
		$this->pea->edit->input->username->setTitle('Username');
		
		$this->pea->edit->addInput('password','password');
		$this->pea->edit->input->password->setTitle('Password');
		$this->pea->edit->input->password->setEncryption('encrypt_password');
		
		$this->pea->edit->addInput('role_id','selecttable');
		$this->pea->edit->input->role_id->setTitle('Group');
		$this->pea->edit->input->role_id->setReferenceTable($this->role_table);
		$this->pea->edit->input->role_id->setReferenceField($this->role_field_name,$this->role_primary);
		
		$this->pea->edit->addInput('name','text');
		$this->pea->edit->input->name->setTitle('Nama');
		
		$this->pea->edit->addInput('email','text');
		$this->pea->edit->input->email->setTitle('Email');
		
		$this->pea->edit->addInput('address','textarea');
		$this->pea->edit->input->address->setTitle('Alamat');
		$this->pea->edit->input->address->setSize(4,60);
		
		$this->pea->edit->addInput('phone','text');
		$this->pea->edit->input->phone->setTitle('Telepon');
		
		$this->pea->edit->addInput('office_phone','text');
		$this->pea->edit->input->office_phone->setTitle('Telepon kantor');
		
		$this->pea->edit->addInput('image','file');
		$this->pea->edit->input->image->setTitle('Foto');
		$this->pea->edit->input->image->setFolder("files/modules/member/",base_url()."files/modules/member/");
		#$this->pea->edit->input->image->setResize(100,100);
		
		$this->pea->edit->addInput('created','hidden');
		$this->pea->edit->input->created->setDefaultValue(date('Y-m-d H:i:s'));
		
		$this->pea->edit->action();		
		$tabs['Tambah user'] = $this->pea->edit->getForm();
		
		$this->tabs->add_tabs($tabs);
		$form = $this->tabs->display();
		
		$data['form'] = $form;
		$data['page'] = $this->general_form;
		$this->load->view($this->layout_content,$data);
			
	}
		
	function edit()
	{
		$this->system->add_breadcrumb('Data user','_cpanel/admin/user/data');
		$this->system->add_breadcrumb('Edit user');
		
		$id = (int)$_GET['id'];
		
		$this->pea->setTable($this->user_table);
		$this->pea->initEdit("WHERE id = $id");
		
		$this->pea->edit->addInput("header","header");
		$this->pea->edit->input->header->setTitle("Edit user");
		
		$this->pea->edit->addInput('username','text');
		$this->pea->edit->input->username->setTitle('Username');
		$this->pea->edit->input->username->setPlaintext(true);
		
		/*$this->pea->edit->addInput('password','password');
		$this->pea->edit->input->password->setTitle('Password');
		$this->pea->edit->input->password->setEncryption('encrypt_password');*/
		
		$this->pea->edit->addInput('role_id','selecttable');
		$this->pea->edit->input->role_id->setTitle('Group');
		$this->pea->edit->input->role_id->setReferenceTable($this->role_table);
		$this->pea->edit->input->role_id->setReferenceField($this->role_field_name,$this->role_primary);
		
		$this->pea->edit->addInput('email','text');
		$this->pea->edit->input->email->setTitle('Email');
		
		$this->pea->edit->addInput('name','text');
		$this->pea->edit->input->name->setTitle('Nama');
		
		$this->pea->edit->addInput('email','text');
		$this->pea->edit->input->email->setTitle('Email');
		
		$this->pea->edit->addInput('address','textarea');
		$this->pea->edit->input->address->setTitle('Alamat');
		$this->pea->edit->input->address->setSize(4,60);
		
		$this->pea->edit->addInput('phone','text');
		$this->pea->edit->input->phone->setTitle('No. Telepon');
		
		$this->pea->edit->addInput('office_phone','text');
		$this->pea->edit->input->office_phone->setTitle('Telepon kantor');
		
		$this->pea->edit->addInput('image','file');
		$this->pea->edit->input->image->setTitle('Foto');
		$this->pea->edit->input->image->setFolder("files/modules/member/",base_url()."files/modules/member/");
		#$this->pea->edit->input->image->setResize(100,100);
		
		$this->pea->edit->addInput('modified','hidden');
		$this->pea->edit->input->modified->setDefaultValue(date('Y-m-d H:i:s'));
		
		$this->pea->edit->action();		
		$form = $this->pea->edit->getForm();
		
		$data['form'] = $form;
		$data['page'] = $this->general_form;
		$this->load->view($this->layout_content,$data);
	}
	
	function groups()
	{
		check_user( uri_string() );
		$this->system->add_breadcrumb('Data group');
		
		$this->pea->setTable($this->role_table);
		$this->pea->initRoll("ORDER BY `name` ASC");
		
		$this->pea->roll->addInput("header","header");
		$this->pea->roll->input->header->setTitle("Data Group");
		
		$this->pea->roll->addInput('name','text');
		$this->pea->roll->input->name->setTitle('Nama group');
		$this->pea->roll->input->name->setPlaintext(true);
		
		$this->pea->roll->addInput("group_id","editlinks");
		$this->pea->roll->input->group_id->setFieldName("id AS group_id");
		$this->pea->roll->input->group_id->setTitle("Permission");
		$this->pea->roll->input->group_id->setCaption("<i class=\"glyphicon glyphicon-lock\"></i>Permission");
		$this->pea->roll->input->group_id->setLinks( site_url('_cpanel/admin/user/permission') );
		
		$this->pea->roll->addInput("id","editlinks");
		$this->pea->roll->input->id->setTitle("Edit");
		$this->pea->roll->input->id->setCaption("<i class=\"glyphicon glyphicon-edit\"></i>Edit");
		$this->pea->roll->input->id->setLinks( site_url('_cpanel/admin/user/edit_group') );
		
		$this->pea->roll->action();		
		$roll = $this->pea->roll->getForm();
		
		$this->pea->initEdit();
		
		$this->pea->edit->addInput("header","header");
		$this->pea->edit->input->header->setTitle("Data Group");
		
		$this->pea->edit->addInput('name','text');
		$this->pea->edit->input->name->setTitle('Nama group');
		
		$this->pea->edit->action();		
		$edit = $this->pea->edit->getForm();
		
		$tabs['Data group'] = $roll;
		$tabs['Tambah Group'] = $edit;
		
		$this->tabs->add_tabs($tabs);
		$form = $this->tabs->display();
		
		$data['form'] = $form;
		$data['page'] = $this->general_form;
		$this->load->view($this->layout_content,$data);
	}
	
	function edit_group()
	{
		$this->system->add_breadcrumb('Data group','_cpanel/admin/user/groups');
		$this->system->add_breadcrumb('Edit group');
		
		$id = (int)$_GET['id'];
		
		$this->pea->setTable($this->role_table);
		$this->pea->initEdit("WHERE id = $id");
		
		$this->pea->edit->addInput("header","header");
		$this->pea->edit->input->header->setTitle("Data Group");
		
		$this->pea->edit->addInput('name','text');
		$this->pea->edit->input->name->setTitle('Edit group');
		
		$this->pea->edit->action();		
		$form = $this->pea->edit->getForm();
		
		$data['form'] = $form;
		$data['page'] = $this->general_form;
		$this->load->view($this->layout_content,$data);
	}
	
	function permission()
	{
		$this->system->add_breadcrumb('Data group','_cpanel/admin/user/groups');
		$this->system->add_breadcrumb('Edit permission');
		
		$groupId = (int)$_GET['id'];
		
		
		$allresources = $this->adodb->GetAll("SELECT id,name FROM sys_resources");
		$r_resources = array();
		
		foreach((array)$allresources as $sources){
			$r_resources[$sources['id']] = $sources['name'];	
		}
		
		$arrRule = array();
		if( isset($_POST['change_permission']) )
		{
			$post_allow = $_POST['allow'];
			foreach((array)$post_allow as $id=>$allow)
			{
				$is_allow = ( $allow == 1 )?TRUE:FALSE;
				$permission_name = $r_resources[$id];
				$this->permissions->set_permission_value($groupId, $permission_name, $is_allow);
			}			
			
			$data['msg'] = "<div class=\"message success-msg\">Permission berhasil disimpan</div>";

		}
				
		$permission_data = $this->permissions->get_permission_data($groupId); 
		$this->load->config('cpanel');
		
		$data['resources'] = array_resources(); 
		$data['permission'] = $permission_data;
		$data['group_id'] = $groupId;
		$data['module'] = '_cpanel';
		$data['page'] = 'layout_permission';
		$this->load->view($this->layout_content,$data);
		
	}
	
	function account()
	{
		#$this->system->add_breadcrumb('Data user','user/admin/user/data');
		$this->system->add_breadcrumb('Edit user');
		
		$id = (int)$this->user->user_id;
		
		$this->pea->setTable($this->user_table);
		$this->pea->initEdit("WHERE id = $id");
		
		$this->pea->edit->addInput("header","header");
		$this->pea->edit->input->header->setTitle("Edit user");
		
		$this->pea->edit->addInput('username','text');
		$this->pea->edit->input->username->setTitle('Username');
		$this->pea->edit->input->username->setPlaintext(true);
		
		$this->pea->edit->addInput('email','text');
		$this->pea->edit->input->email->setTitle('Email');
		
		$this->pea->edit->addInput('name','text');
		$this->pea->edit->input->name->setTitle('Nama');
		
		$this->pea->edit->addInput('email','text');
		$this->pea->edit->input->email->setTitle('Email');
		
		$this->pea->edit->addInput('address','textarea');
		$this->pea->edit->input->address->setTitle('Alamat');
		$this->pea->edit->input->address->setSize(4,60);
		
		$this->pea->edit->addInput('phone','text');
		$this->pea->edit->input->phone->setTitle('No. Telepon');
		
		$this->pea->edit->addInput("description","textarea");
		$this->pea->edit->input->description->setTitle("Deskripsi");
		$this->pea->edit->input->description->setHtmlEditor(true);
		
		$this->pea->edit->addInput('image','file');
		$this->pea->edit->input->image->setTitle('Foto');
		$this->pea->edit->input->image->setFolder("files/modules/agent/",base_url()."files/modules/agent/");
		$this->pea->edit->input->image->setResize(300,300);
		
		$this->pea->edit->addInput('modified','hidden');
		$this->pea->edit->input->modified->setDefaultValue(date('Y-m-d H:i:s'));
		
		$this->pea->edit->action();		
		$form = $this->pea->edit->getForm();
		
		$data['form'] = $form;
		$data['page'] = $this->general_form;
		$this->load->view($this->layout_content,$data);
	}
	
	function password()
	{
		$this->system->add_breadcrumb('Ubah password');
		$id = (int)$this->user->user_id;
		
		$report = "";
		if( isset($_POST['change_password']) )
		{
			$old_password = $_POST['password_old'];
			$new_password = $_POST['password_new'];
			$ret_password = $_POST['password_con'];	
			
			$this->form_validation->set_rules('password_old', 'Password lama', 'required');
			$this->form_validation->set_rules('password_new', 'Password baru', 'required|matches[password_con]|min_length[6]');
			$this->form_validation->set_rules('password_con', 'Konfirmasi password', 'required|min_length[6]');
			
			
			if( $this->form_validation->run() == FALSE )
			{
				$error = validation_errors();
				$report = "<div class=\"message error-msg\">$error</div>";	
			}
			else
			{
				$status = $this->dx_auth->change_password($old_password,$new_password);
				if( $status == TRUE )
				{
					$report = "<div class=\"message success-msg\">Password berhasil diubah</div>";
				}
				else
				{
					$message = $this->dx_auth->get_auth_error();
					$report = "<div class=\"message error-msg\">$message</div>";
				}
			}
			
		}
		
		$data['report'] = $report;
		$data['form'] = $id;
		$data['page'] = 'layout_change_password';
		$this->load->view($this->layout_content,$data);	
	}
}