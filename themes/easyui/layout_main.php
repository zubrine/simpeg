<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title><?php echo $title; ?></title>
<script type="text/javascript">
var BASE_URL = '<?php echo base_url();?>';
var THEME_URL = '<?php echo $this->theme_url;?>';
</script>
<?php
$this->meta->css($this->theme_url.'css/jeans.css');
$this->meta->css($this->theme_url.'css/custom.css');
$this->meta->css(_ASSET_URL.'icons/css/font-awesome.min.css');
$this->meta->js($this->theme_url.'js/jquery-1.8.0.min.js');
$this->meta->js($this->theme_url.'js/jquery.easyui.min.js');
$this->meta->js($this->theme_url.'js/jquery.validate.min.js');
echo $this->meta->display( FALSE );
?>

</head>
<body>
<div class="x-panel" fit="true" border="false" bodyCls="panel-bordered" style="padding:2px;">

		<div class="x-layout" fit="true" border="false">
        	<div region="north" border="false" split="false" style="background: transparent; height:50px; border-bottom:3px solid #4A89DC;">
            xcxzczxc
            	<div class="head-left">Logo</div>
                <div class="head-right">
                    <a href="<?php echo site_url('auth/logout');?>" class="l-btn l-btn-small" group="">
                        <span class="l-btn-left l-btn-icon-left">
                            <span class="l-btn-text">Logout</span>
                            <span class="l-btn-icon icon-logout">&nbsp;</span>
                        </span>
                     </a>
                </div>
                <div style="clear:both"></div>
            </div>
			<div region="west" border="false" split="true" style="width:233px;" data-options="collapsible:false">
				
               
                
                <div class="x-panel" fit="true" border="false">
                    <div class="x-accordion" fit="true">
                        <div title="Main menu">
                        	<ul class="x-tree" lines="true" id="tree" data-options="url:'<?=site_url("main/access")?>'"></ul>
                        </div>
                        <div title="Settings"></div>
                    </div>
                </div>
                
			</div>
			<div region="center" border="false">
				
				<div class="x-tabs" id="tabs" fit="true" border="false" tabWidth="150">
					<div title="Example">
						<div>Homeee</div>
					</div>
				</div>
                
                

			</div>
		</div>

</div>

<script type="text/javascript">
	$('#tree').tree({
		onClick: function (e) {			
			if ($('#tree').tree('isLeaf', e.target)) {
				if ($('#tabs').tabs('exists', e.text)) {
					$('#tabs').tabs('select', e.text);
				} else {					
					var name = e.text.toLowerCase();
					var url = e.url;
					var content = '<iframe frameborder="0" width="100%" height="100%" src="'+url+'"></iframe>';
					$('#tabs').tabs('add', {
						title: e.text,
						closable:true,
						//href:url						
						content:content
					});						
				}
			};
		}
	});
	//$(window).resize();	
</script>

</body>
</html>