<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<script type="text/javascript">
var BASE_URL = '<?php echo base_url();?>';
var THEME_URL = '<?php echo $this->theme_url;?>';
</script>
<?php

$this->meta->css($this->theme_url.'css/jeans.css');
$this->meta->css($this->theme_url.'login/css/style.css');
$this->meta->js($this->theme_url.'js/jquery-1.8.0.min.js');
$this->meta->js($this->theme_url.'login/js/modernizr.custom.63321.js');
$this->meta->js($this->theme_url.'js/jquery.easyui.min.js');
$this->meta->js($this->theme_url.'js/jquery.validate.min.js');
$this->meta->js($this->theme_url.'login/js/login.js');
echo $this->meta->display( FALSE );
?>
</head>

<body>
<div class="container">		
			
    <section class="main">
        <form class="form-2" method="post" action="<?php echo site_url('auth/login'); ?>" id="frmlogin">
            <h1><span class="log-in">Log in</span> <span class="sign-up">pengguna</span></h1>
            <p><div id="responsemsg"></div></p>
            <p>
                <label for="login"><i class="icon-user"></i>USER ID</label>
                <input type="text" name="username" id="username" placeholder="User ID">
            </p>
            <p>
                <label for="password"><i class="icon-lock"></i>Password</label>
                <input type="password" name="password" id="password" placeholder="Password">
            </p>
            <p class="clearfix">
                <br />    
                <input type="submit" name="submit" value="Log in">
            </p>
        </form>​​
    </section>
    
</div>
</body>
</html>