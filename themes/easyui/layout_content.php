<?php
if( isset($page) ) 
{	
	if( isset($module) and !empty($module) )
	{
		if( file_exists(_ROOT."modules/$module/views/".$page.".php") ){
			include _ROOT . "modules/$module/views/".$page.".php";
		}else{
			echo "File : "._ROOT."modules/$module/views/".$page.".php tidak ditemukan";	
		}
	}else{
		
		if( file_exists(_ROOT."themes/".$page.".php") ){
			include _ROOT . "themes/".$page.".php";
		}else{
			echo "File : "._ROOT."themes/".$page.".php tidak ditemukan";	
		}
		
	}
}
?>