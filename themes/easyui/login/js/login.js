// JavaScript Document
// 26Maret2014 14:21

$(document).ready(function() {
    $("#frmlogin").validate({
		//errorElement: "div",
		//errorClass: "errorJSHome",
		rules: {
			username: {
				required: true
			},
			password: {
				required: true
			}
		},
		messages: {
			username: {
				required: '<span class="lnotif-error">Masukkan User ID Anda.</span>',
			},
			password: {
				required: '<span class="lnotif-error">Masukan Password Anda.</span>',
			}
		},
		submitHandler: function() {
				$.ajax({
				type: 'POST',
				url: $('#frmlogin').attr('action'),
				data: $('#frmlogin').serialize(),
				datatype: 'json',
				//timeout: 20000,
				error: function(data) {
					alert(data.msg);
				},
				success: function (data) {
					if(data.status==1){
						if(data.redirect){
							document.location.href=data.redirect;	
						}
					}else{
						alert(data.msg);	
					}
				}
			});
			return false;
		}
	});
});