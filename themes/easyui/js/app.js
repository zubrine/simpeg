var key = "tabs";
var value = window.localStorage.getItem(key);
if (value == null) {
	window.localStorage.setItem(key, "[]");
};
value = eval(value);
// value.push({text:"Tabs 1"});
// value.shift({text:"Tabs 1"});
window.localStorage.setItem(key, JSON.stringify(value));
$("#tree-home").tree({
	onClick: function(e) {
		console.log(e);
	}
});
$("#tree-arsip").tree({
	onClick: function(e) {
		console.log(e);
	}
});
$("#tree-pengaturan").tree({
	onClick: function(e) {
		console.log(e);
	}
});
$("#datagrid-penduduk").datagrid({
	detailView: true,
	remoteSort: false,
	columns: [
		[
				{field: "nik", title: "NIK", sortable:"true", width: 100, sorter:sorter},
				{field: "nama", title: "Nama", sortable:"true", width: 100, sorter:sorter},
				// {field: "tempat_lahir", title: "Tempat Lahir", sortable:"true", width: 100},
				// {field: "tanggal_lahir", title: "Tanggal Lahir", sortable:"true", width: 100},
				// {field: "jenis_kelamin", title: "Jenis Kelamin", sortable:"true", width: 100},
				// {field: "golongan_darah", title: "Golongan Darah", sortable:"true", width: 100},
				{field: "alamat", title: "Alamat", sortable:"true", width: 100, sorter:sorter},
				// {field: "rt", title: "Rt", sortable:"true", width: 100},
				// {field: "rw", title: "Rw", sortable:"true", width: 100},
				{field: "kelurahan", title: "Kelurahan", sortable:"true", width: 100, sorter:sorter},
				{field: "kecamatan", title: "Kecamatan", sortable:"true", width: 100, sorter:sorter},
				// {field: "agama", title: "Agama", sortable:"true", width: 100},
				// {field: "status", title: "Status", sortable:"true", width: 100},
				// {field: "pekerjaan", title: "Pekerjaan", sortable:"true", width: 100},
				// {field: "kewarganegaraan", title: "Kewarganegaraan", sortable:"true", width: 100},
				{field: "nomor_akta", title: "Nomor Akta", sortable:"true", width: 100, sorter:sorter},
			]
	],
	data: [
		{
			"nik": "Excepturi",
			"nama": "Asperiores",
			"tempat_lahir": "Doloribus",
			"tanggal_lahir": "Mollitia",
			"jenis_kelamin": "Soluta",
			"golongan_darah": "Accusantium",
			"alamat": "Sunt",
			"rt": "Magni",
			"rw": "Quas",
			"kelurahan": "Necessitatibus",
			"kecamatan": "Ea",
			"agama": "Non",
			"status": "Eum",
			"pekerjaan": "Quis",
			"kewarganegaraan": "Quae",
			"nomor_akta": "Ut",
		},
		{
			"nik": "Provident",
			"nama": "Harum",
			"tempat_lahir": "Optio",
			"tanggal_lahir": "Ullam",
			"jenis_kelamin": "Atque",
			"golongan_darah": "Eligendi",
			"alamat": "Quo",
			"rt": "Cumque",
			"rw": "Blanditiis",
			"kelurahan": "Officiis",
			"kecamatan": "Iste",
			"agama": "Alias",
			"status": "Consequatur",
			"pekerjaan": "Cupiditate",
			"kewarganegaraan": "Placeat",
			"nomor_akta": "Nihil",
		},
		{
			"nik": "Dolores",
			"nama": "Accusamus",
			"tempat_lahir": "Sapiente",
			"tanggal_lahir": "Perspiciatis",
			"jenis_kelamin": "Animi",
			"golongan_darah": "Accusamus",
			"alamat": "Ullam",
			"rt": "Hic",
			"rw": "A",
			"kelurahan": "Iusto",
			"kecamatan": "Aliquid",
			"agama": "Recusandae",
			"status": "Ab",
			"pekerjaan": "Quidem",
			"kewarganegaraan": "Voluptatum",
			"nomor_akta": "Dolore",
		},
		{
			"nik": "Dolor",
			"nama": "Itaque",
			"tempat_lahir": "Illo",
			"tanggal_lahir": "In",
			"jenis_kelamin": "Iste",
			"golongan_darah": "Pariatur",
			"alamat": "Asperiores",
			"rt": "Tempore",
			"rw": "Laboriosam",
			"kelurahan": "Aperiam",
			"kecamatan": "A",
			"agama": "Facilis",
			"status": "Eius",
			"pekerjaan": "Velit",
			"kewarganegaraan": "Repellendus",
			"nomor_akta": "Ex",
		},
		{
			"nik": "Incidunt",
			"nama": "Ducimus",
			"tempat_lahir": "Natus",
			"tanggal_lahir": "Distinctio",
			"jenis_kelamin": "Dolorum",
			"golongan_darah": "Pariatur",
			"alamat": "Architecto",
			"rt": "Repellat",
			"rw": "Laudantium",
			"kelurahan": "Voluptates",
			"kecamatan": "Quasi",
			"agama": "Vitae",
			"status": "Recusandae",
			"pekerjaan": "Vero",
			"kewarganegaraan": "Alias",
			"nomor_akta": "Laudantium",
		},
		{
			"nik": "Laborum",
			"nama": "Quas",
			"tempat_lahir": "Unde",
			"tanggal_lahir": "At",
			"jenis_kelamin": "Laborum",
			"golongan_darah": "Consectetur",
			"alamat": "Debitis",
			"rt": "Minima",
			"rw": "Expedita",
			"kelurahan": "Delectus",
			"kecamatan": "Iusto",
			"agama": "Dignissimos",
			"status": "Debitis",
			"pekerjaan": "In",
			"kewarganegaraan": "Dolorem",
			"nomor_akta": "Eos",
		},
		{
			"nik": "Veniam",
			"nama": "Impedit",
			"tempat_lahir": "Culpa",
			"tanggal_lahir": "Ex",
			"jenis_kelamin": "Alias",
			"golongan_darah": "Facere",
			"alamat": "Recusandae",
			"rt": "Veritatis",
			"rw": "Ipsa",
			"kelurahan": "Mollitia",
			"kecamatan": "Minima",
			"agama": "Nulla",
			"status": "Eligendi",
			"pekerjaan": "Animi",
			"kewarganegaraan": "Et",
			"nomor_akta": "Necessitatibus",
		},
		{
			"nik": "Cumque",
			"nama": "Culpa",
			"tempat_lahir": "Ea",
			"tanggal_lahir": "Voluptatum",
			"jenis_kelamin": "Recusandae",
			"golongan_darah": "Animi",
			"alamat": "Ex",
			"rt": "Expedita",
			"rw": "Nemo",
			"kelurahan": "Consequuntur",
			"kecamatan": "Totam",
			"agama": "Eveniet",
			"status": "Harum",
			"pekerjaan": "Quae",
			"kewarganegaraan": "Molestias",
			"nomor_akta": "Laboriosam",
		},
		{
			"nik": "Laudantium",
			"nama": "Esse",
			"tempat_lahir": "Quibusdam",
			"tanggal_lahir": "Nisi",
			"jenis_kelamin": "Similique",
			"golongan_darah": "Necessitatibus",
			"alamat": "Odit",
			"rt": "Rerum",
			"rw": "Praesentium",
			"kelurahan": "Earum",
			"kecamatan": "Unde",
			"agama": "Repellendus",
			"status": "Tempore",
			"pekerjaan": "Odio",
			"kewarganegaraan": "Sapiente",
			"nomor_akta": "Adipisci",
		},
		{
			"nik": "Eos",
			"nama": "Non",
			"tempat_lahir": "Tenetur",
			"tanggal_lahir": "Dolorum",
			"jenis_kelamin": "Iste",
			"golongan_darah": "Quia",
			"alamat": "Explicabo",
			"rt": "Fugit",
			"rw": "Quidem",
			"kelurahan": "Quod",
			"kecamatan": "Voluptatibus",
			"agama": "Nobis",
			"status": "Dolor",
			"pekerjaan": "Soluta",
			"kewarganegaraan": "Fugiat",
			"nomor_akta": "Voluptatibus",
		},
	],
	detailFormatter: function(e, f) {
		return '<div style="padding:5px;">' +
		'<div class="row"><div class="col-3"><strong>Tempat Lahir:</strong></div><div class="col-3">'+f.tempat_lahir+'</div></div>' +
		'<div class="row"><div class="col-3"><strong>Tanggal Lahir:</strong></div><div class="col-3">'+f.tanggal_lahir+'</div></div>' +
		'<div class="row"><div class="col-3"><strong>Jenis Kelamin:</strong></div><div class="col-3">'+f.jenis_kelamin+'</div></div>' +
		'<div class="row"><div class="col-3"><strong>Golongan Darah:</strong></div><div class="col-3">'+f.golongan_darah+'</div></div>' +
		'<div class="row"><div class="col-3"><strong>Rt:</strong></div><div class="col-3">'+f.rt+'</div></div>' +
		'<div class="row"><div class="col-3"><strong>Rw:</strong></div><div class="col-3">'+f.rw+'</div></div>' +
		'<div class="row"><div class="col-3"><strong>Agama:</strong></div><div class="col-3">'+f.agama+'</div></div>' +
		'<div class="row"><div class="col-3"><strong>Status:</strong></div><div class="col-3">'+f.status+'</div></div>' +
		'<div class="row"><div class="col-3"><strong>Pekerjaan:</strong></div><div class="col-3">'+f.pekerjaan+'</div></div>' +
		'<div class="row"><div class="col-3"><strong>Kewarganegaraan:</strong></div><div class="col-3">'+f.kewarganegaraan+'</div></div>' +
		'</div>';
	}
});

function sorter(a,b){  
    a = a.split('/');  
    b = b.split('/');  
    if (a[2] == b[2]){  
        if (a[0] == b[0]){  
            return (a[1]>b[1]?1:-1);  
        } else {  
            return (a[0]>b[0]?1:-1);  
        }  
    } else {  
        return (a[2]>b[2]?1:-1);  
    }  
}