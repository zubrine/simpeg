var ls = window.localStorage;
var getTabs = eval(ls.getItem('tabs'));
$('#tree').tree({
    onClick: function(node) {
        var tabs = ls.getItem("tabs");
        if (tabs == null) {
            ls.setItem("tabs", "[]");
        };
        var insTabs = eval(tabs);
        insTabs.push({"name":node.text});
        if ($("#tabs").tabs("exists", node.text)) {
            $("#tabs").tabs("select", node.text);
        } else {
            $("#tabs").tabs("add", {
                title: node.text,
                closable: true
            });
            ls.setItem("tabs", JSON.stringify(insTabs));
        }
    }
});
$('#tabs').tabs({
    onClose: function(e) {
        var tabs = eval(ls.getItem("tabs"));
        tabs.shift({"name":e});
        ls.setItem("tabs", JSON.stringify(tabs));
    }
});
$(function(arguments) {
    for(i in getTabs) {
        if ($("#tabs").tabs("exists", getTabs[i].name)) {
        } else {
            $("#tabs").tabs("add", {
                title:getTabs[i].name,
                closable: true
            });
        }
    }
});