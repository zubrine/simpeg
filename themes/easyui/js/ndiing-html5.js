String.prototype.quote = function(){
 return '"'+this.replace(/(^|[^\\])"/g,'$1\\"')+'"';
}
String.prototype.capitalize = function() {
    return this.replace(/(?:^|\s)\S/g, function(a) { return a.toUpperCase(); });
};
function array_unique (inputArr) {
  var key = '',
  tmp_arr2 = {},
  val = '';
  var __array_search = function (needle, haystack) {
    var fkey = '';
    for (fkey in haystack) {
      if (haystack.hasOwnProperty(fkey)) {
        if ((haystack[fkey] + '') === (needle + '')) {
          return fkey;
        }
      }
    }
    return false;
  };
  for (key in inputArr) {
    if (inputArr.hasOwnProperty(key)) {
      val = inputArr[key];
      if (false === __array_search(val, tmp_arr2)) {
        tmp_arr2[key] = val;
      }
    }
  }
  return tmp_arr2;
}
var WebSQL = function(defaults) {
	var websql = {
		defaults: {
			name: '',
			size: 5242880,
			description: '',
			version: 1
		},
		_query: new Array(),
		_select: new Array(),
		_from: new Array(),
		_where: new Array(),
		_join: new Array(),
		_joinType: ["LEFT","RIGHT","OUTER","INNER","LEFT OUTER","RIGHT OUTER"],
		_whereOperator: ["=","!=",">","<",">=","<="],
		init: function(defaults) {
			if (defaults && typeof defaults == "string") {
				this.defaults.name = defaults;
			} else {
				this.defaults = $.extend({}, this.defaults, defaults);
			}
			try {
				if (!window.openDatabase) {
					throw "Error, Not Support Web SQL!";
				} else {
					this._db = openDatabase(this.defaults.name, this.defaults.version, this.defaults.description, this.defaults.size);
				}
			} catch(e) {
				alert(e.message);
			}
			return this;
		},
		db: function(query, param, success, error) {
			return this._db.transaction(function(_transaction) {
				_transaction.executeSql(query, param, success, error);
			});
		},
		select: function(column) {
			column = column || "*";
			this._select.push(column);
			return this;
		},
		from: function(table) {
			this._from.push(table);
			return this;
		},
		where: function(key, operator, value) {
			value = value || operator;
			if (this._whereOperator.indexOf(operator) == -1) {
				operator = "=";
			};
			if (typeof value === "string") {
				value = value.quote();
			};
			this._where.push([key, operator, value].join(" "));
			return this;
		},
		join: function(table, param, type) {
			type = type !== undefined ? type.toUpperCase() : "";
			var _join = [type,"JOIN",table, "ON", param];
			if (this._joinType.indexOf(type) == -1) {
				_join = _join.slice(1);
			};
			this._join.push(_join.join(" "));
			return this;
		},
		_selectQuery: function() {
			this._query.push("SELECT");
			if (this._select.length) {
				this._query.push(this._select.join(", "));
			} else {
				this._query.push("*");
			}
		},
		_fromQuery: function() {
			this._query.push("FROM");
			this._query.push(this._from.join(", "));
		},
		_whereQuery: function() {
			if (this._where.length) {
				for (var i = 0; i < this._where.length; i++) {
					if (i == 0) {
						this._query.push("WHERE", this._where[i]);
					} else {
						this._query.push("AND", this._where[i]);
					}
				};
			};
		},
		_joinQuery: function() {
			if (this._join.length) {
				this._query.push(this._join.join(" "));
			};
		},
		get: function(table, limit, offset) {
			if (table) {
				this._from.push(table);
			};
			this._selectQuery();
			this._fromQuery();
			this._joinQuery();
			this._whereQuery();
			return this;
		},
		result: function(success, error) {
			var query = this._query.join(" ");
			this.query(query, function(value) {
				var rows = value.rows;
				var _result = [];
				for (var i = 0; i < rows.length; i++) {
					_result.push(rows.item(i));
				};
				if (success && typeof success === "function") {
					success.call(this, _result);
				};
			}, error);
			this._resetQuery();
		},
		_resetQuery: function() {
			this._query = [];
			this._select = [];
			this._from = [];
			this._where = [];
			this._join = [];
		},
		insert: function(table, data, success, error) {
			var columns = [];
			var values = [];
			for(var i in data) {
				if (typeof data[i] === "string") {
					data[i] = data[i].quote();
				};
				columns.push(i);
				values.push(data[i]);
			}
			this._query.push(["INSERT INTO",table,"(",columns.join(", "),")","VALUES","(",values.join(", "),")"].join(" "));
			var query = this._query.join(" ");
			this.query(query, success, error);
			this._resetQuery();
		},
		insertBatch: function(table, data, success, error) {
			for(var i in data) {
				this.insert(table, data[i], success, error);
			}
		},
		update: function(table, data, success, error) {
			var _set = [];
			for(var i in data) {
				if (typeof data[i] === "string") {
					data[i] = data[i].quote();
				};
				_set.push([i,"=", data[i]].join(" "));
			}
			this._query.push(["UPDATE",table,"SET",_set.join(", ")].join(" "));
			this._whereQuery();
			var query = this._query.join(" ");
			this.query(query, success, error);
			this._resetQuery();
		},
		delete: function(table, success, error) {
			this._query.push(["DELETE FROM", table].join(" "));
			this._whereQuery();
			var query = this._query.join(" ");
			this.query(query, success, error);
			this._resetQuery();
		},
		query: function(_query, success, error) {
			this.db(_query, [], function(_transaction, _result) {
				if (success && typeof success === "function") {
					success.call(_transaction, _result)
				};
			}, function(_transaction, _error) {
				error.call(_transaction, _error.message);
			});
		}
	};
	return Object.create(websql).init(defaults);
}
// var db = new WebSQL("capil");

// test query

// var query1 = db.get("tableA").result();
// var query2 = db.from('tableA').get().result();
// var query3 = db.select("id, name, email").get("tableA").result();
// var query4 = db.select("id").select("name").select("email").get("tableA").result();
// var query5 = db.where("idA", 1).where("idB","!=", 1).where("idC", "book").get("tableA").result();
// var query6 = db.join('book', 'book.id = room.id').get("tableA").result();
// var query7 = db.join('book', 'book.id = room.id', 'left').get("tableA").result();
// var query8 = db.join('book', 'book.id = room.id')
// 			   .join('push', 'push.id = pull.id').get("tableA").result();
// var data1 = {
// 	"id": 1,
// 	"name": "ndiing",
// 	"email": "ndiing@me.com"
// };
// var query9 = db.insert("table", data1);
// var data2 = {
// 	"id": 1,
// 	"name": "ndiing",
// 	"email": "ndiing@me.com"
// };
// var query10 = db.where("id", 1).update("table", data2);
// var query11 = db.where("id", 1).delete("table");

// test transaction query

// db.query("SELECT * FROM table", function(e) {
// 	console.log(e);
// }, function(e) {
// 	console.log(e);
// });

// db.query("SELECT * FROM agama", function(e) {
// 	console.log(e);
// }, function(e) {
// 	console.log(e);
// });

// db.get('agama').result(function(e) {
// 	console.log(e);
// });
// db.where('id_agama', 1).get('agama').result(function(e) {
// 	console.log(e);
// });
// db.where('id_agama', '!=', 1).get('agama').result(function(e) {
// 	console.log(e);
// });

// var data1 = {
// 	"nama_agama": "agama"
// };
// db.insert('agama', data1);

// var data1 = {
// 	"nama_agama": "agama lagi"
// };
// db.where('id_agama', 9).update('agama', data1, function(success) {
	// }, function(error) {
// });

// db.where('id_agama', 9).delete('agama', function(success) {
	// }, function(error) {
// });

var FileSystem = function(defaults) {
	var filesystem = {
		defaults: {
			type: 'persistent',
			size: 5368709120,
		},
		_storage: undefined,
		_storageType: ["TEMPORARY", "PERSISTENT"],
		_requestStorageType: ["webkitTemporaryStorage", "webkitPersistentStorage"],
		_fileInfo: [
			{"extention":"evy", "type":"application/envoy", "reader": ""},
			{"extention":"fif", "type":"application/fractals", "reader": ""},
			{"extention":"spl", "type":"application/futuresplash", "reader": ""},
			{"extention":"hta", "type":"application/hta", "reader": ""},
			{"extention":"acx", "type":"application/internet-property-stream", "reader": ""},
			{"extention":"hqx", "type":"application/mac-binhex40", "reader": ""},
			{"extention":"doc", "type":"application/msword", "reader": ""},
			{"extention":"dot", "type":"application/msword", "reader": ""},
			{"extention":"*", "type":"application/octet-stream", "reader": ""},
			{"extention":"bin", "type":"application/octet-stream", "reader": ""},
			{"extention":"class", "type":"application/octet-stream", "reader": ""},
			{"extention":"dms", "type":"application/octet-stream", "reader": ""},
			{"extention":"exe", "type":"application/octet-stream", "reader": ""},
			{"extention":"lha", "type":"application/octet-stream", "reader": ""},
			{"extention":"lzh", "type":"application/octet-stream", "reader": ""},
			{"extention":"oda", "type":"application/oda", "reader": ""},
			{"extention":"axs", "type":"application/olescript", "reader": ""},
			{"extention":"pdf", "type":"application/pdf", "reader": ""},
			{"extention":"prf", "type":"application/pics-rules", "reader": ""},
			{"extention":"p10", "type":"application/pkcs10", "reader": ""},
			{"extention":"crl", "type":"application/pkix-crl", "reader": ""},
			{"extention":"ai", "type":"application/postscript", "reader": ""},
			{"extention":"eps", "type":"application/postscript", "reader": ""},
			{"extention":"ps", "type":"application/postscript", "reader": ""},
			{"extention":"rtf", "type":"application/rtf", "reader": ""},
			{"extention":"setpay", "type":"application/set-payment-initiation", "reader": ""},
			{"extention":"setreg", "type":"application/set-registration-initiation", "reader": ""},
			{"extention":"xla", "type":"application/vnd.ms-excel", "reader": ""},
			{"extention":"xlc", "type":"application/vnd.ms-excel", "reader": ""},
			{"extention":"xlm", "type":"application/vnd.ms-excel", "reader": ""},
			{"extention":"xls", "type":"application/vnd.ms-excel", "reader": ""},
			{"extention":"xlt", "type":"application/vnd.ms-excel", "reader": ""},
			{"extention":"xlw", "type":"application/vnd.ms-excel", "reader": ""},
			{"extention":"msg", "type":"application/vnd.ms-outlook", "reader": ""},
			{"extention":"sst", "type":"application/vnd.ms-pkicertstore", "reader": ""},
			{"extention":"cat", "type":"application/vnd.ms-pkiseccat", "reader": ""},
			{"extention":"stl", "type":"application/vnd.ms-pkistl", "reader": ""},
			{"extention":"pot", "type":"application/vnd.ms-powerpoint", "reader": ""},
			{"extention":"pps", "type":"application/vnd.ms-powerpoint", "reader": ""},
			{"extention":"ppt", "type":"application/vnd.ms-powerpoint", "reader": ""},
			{"extention":"mpp", "type":"application/vnd.ms-project", "reader": ""},
			{"extention":"wcm", "type":"application/vnd.ms-works", "reader": ""},
			{"extention":"wdb", "type":"application/vnd.ms-works", "reader": ""},
			{"extention":"wks", "type":"application/vnd.ms-works", "reader": ""},
			{"extention":"wps", "type":"application/vnd.ms-works", "reader": ""},
			{"extention":"hlp", "type":"application/winhlp", "reader": ""},
			{"extention":"bcpio", "type":"application/x-bcpio", "reader": ""},
			{"extention":"cdf", "type":"application/x-cdf", "reader": ""},
			{"extention":"z", "type":"application/x-compress", "reader": ""},
			{"extention":"tgz", "type":"application/x-compressed", "reader": ""},
			{"extention":"cpio", "type":"application/x-cpio", "reader": ""},
			{"extention":"csh", "type":"application/x-csh", "reader": ""},
			{"extention":"dcr", "type":"application/x-director", "reader": ""},
			{"extention":"dir", "type":"application/x-director", "reader": ""},
			{"extention":"dxr", "type":"application/x-director", "reader": ""},
			{"extention":"dvi", "type":"application/x-dvi", "reader": ""},
			{"extention":"gtar", "type":"application/x-gtar", "reader": ""},
			{"extention":"gz", "type":"application/x-gzip", "reader": ""},
			{"extention":"hdf", "type":"application/x-hdf", "reader": ""},
			{"extention":"ins", "type":"application/x-internet-signup", "reader": ""},
			{"extention":"isp", "type":"application/x-internet-signup", "reader": ""},
			{"extention":"iii", "type":"application/x-iphone", "reader": ""},
			{"extention":"js", "type":"application/x-javascript", "reader": ""},
			{"extention":"latex", "type":"application/x-latex", "reader": ""},
			{"extention":"mdb", "type":"application/x-msaccess", "reader": ""},
			{"extention":"crd", "type":"application/x-mscardfile", "reader": ""},
			{"extention":"clp", "type":"application/x-msclip", "reader": ""},
			{"extention":"dll", "type":"application/x-msdownload", "reader": ""},
			{"extention":"m13", "type":"application/x-msmediaview", "reader": ""},
			{"extention":"m14", "type":"application/x-msmediaview", "reader": ""},
			{"extention":"mvb", "type":"application/x-msmediaview", "reader": ""},
			{"extention":"wmf", "type":"application/x-msmetafile", "reader": ""},
			{"extention":"mny", "type":"application/x-msmoney", "reader": ""},
			{"extention":"pub", "type":"application/x-mspublisher", "reader": ""},
			{"extention":"scd", "type":"application/x-msschedule", "reader": ""},
			{"extention":"trm", "type":"application/x-msterminal", "reader": ""},
			{"extention":"wri", "type":"application/x-mswrite", "reader": ""},
			{"extention":"cdf", "type":"application/x-netcdf", "reader": ""},
			{"extention":"nc", "type":"application/x-netcdf", "reader": ""},
			{"extention":"pma", "type":"application/x-perfmon", "reader": ""},
			{"extention":"pmc", "type":"application/x-perfmon", "reader": ""},
			{"extention":"pml", "type":"application/x-perfmon", "reader": ""},
			{"extention":"pmr", "type":"application/x-perfmon", "reader": ""},
			{"extention":"pmw", "type":"application/x-perfmon", "reader": ""},
			{"extention":"p12", "type":"application/x-pkcs12", "reader": ""},
			{"extention":"pfx", "type":"application/x-pkcs12", "reader": ""},
			{"extention":"p7b", "type":"application/x-pkcs7-certificates", "reader": ""},
			{"extention":"spc", "type":"application/x-pkcs7-certificates", "reader": ""},
			{"extention":"p7r", "type":"application/x-pkcs7-certreqresp", "reader": ""},
			{"extention":"p7c", "type":"application/x-pkcs7-mime", "reader": ""},
			{"extention":"p7m", "type":"application/x-pkcs7-mime", "reader": ""},
			{"extention":"p7s", "type":"application/x-pkcs7-signature", "reader": ""},
			{"extention":"sh", "type":"application/x-sh", "reader": ""},
			{"extention":"shar", "type":"application/x-shar", "reader": ""},
			{"extention":"swf", "type":"application/x-shockwave-flash", "reader": ""},
			{"extention":"sit", "type":"application/x-stuffit", "reader": ""},
			{"extention":"sv4cpio", "type":"application/x-sv4cpio", "reader": ""},
			{"extention":"sv4crc", "type":"application/x-sv4crc", "reader": ""},
			{"extention":"tar", "type":"application/x-tar", "reader": ""},
			{"extention":"tcl", "type":"application/x-tcl", "reader": ""},
			{"extention":"tex", "type":"application/x-tex", "reader": ""},
			{"extention":"texi", "type":"application/x-texinfo", "reader": ""},
			{"extention":"texinfo", "type":"application/x-texinfo", "reader": ""},
			{"extention":"roff", "type":"application/x-troff", "reader": ""},
			{"extention":"t", "type":"application/x-troff", "reader": ""},
			{"extention":"tr", "type":"application/x-troff", "reader": ""},
			{"extention":"man", "type":"application/x-troff-man", "reader": ""},
			{"extention":"me", "type":"application/x-troff-me", "reader": ""},
			{"extention":"ms", "type":"application/x-troff-ms", "reader": ""},
			{"extention":"ustar", "type":"application/x-ustar", "reader": ""},
			{"extention":"src", "type":"application/x-wais-source", "reader": ""},
			{"extention":"cer", "type":"application/x-x509-ca-cert", "reader": ""},
			{"extention":"crt", "type":"application/x-x509-ca-cert", "reader": ""},
			{"extention":"der", "type":"application/x-x509-ca-cert", "reader": ""},
			{"extention":"pko", "type":"application/ynd.ms-pkipko", "reader": ""},
			{"extention":"zip", "type":"application/zip", "reader": ""},
			{"extention":"au", "type":"audio/basic", "reader": ""},
			{"extention":"snd", "type":"audio/basic", "reader": ""},
			{"extention":"mid", "type":"audio/mid", "reader": ""},
			{"extention":"rmi", "type":"audio/mid", "reader": ""},
			{"extention":"mp3", "type":"audio/mpeg", "reader": ""},
			{"extention":"aif", "type":"audio/x-aiff", "reader": ""},
			{"extention":"aifc", "type":"audio/x-aiff", "reader": ""},
			{"extention":"aiff", "type":"audio/x-aiff", "reader": ""},
			{"extention":"m3u", "type":"audio/x-mpegurl", "reader": ""},
			{"extention":"ra", "type":"audio/x-pn-realaudio", "reader": ""},
			{"extention":"ram", "type":"audio/x-pn-realaudio", "reader": ""},
			{"extention":"wav", "type":"audio/x-wav", "reader": ""},
			{"extention":"bmp", "type":"image/bmp", "reader": "readAsDataURL"},
			{"extention":"cod", "type":"image/cis-cod", "reader": "readAsDataURL"},
			{"extention":"gif", "type":"image/gif", "reader": "readAsDataURL"},
			{"extention":"ief", "type":"image/ief", "reader": "readAsDataURL"},
			{"extention":"jpe", "type":"image/jpeg", "reader": "readAsDataURL"},
			{"extention":"jpeg", "type":"image/jpeg", "reader": "readAsDataURL"},
			{"extention":"jpg", "type":"image/jpeg", "reader": "readAsDataURL"},
			{"extention":"jfif", "type":"image/pipeg", "reader": "readAsDataURL"},
			{"extention":"svg", "type":"image/svg+xml", "reader": "readAsDataURL"},
			{"extention":"tif", "type":"image/tiff", "reader": "readAsDataURL"},
			{"extention":"tiff", "type":"image/tiff", "reader": "readAsDataURL"},
			{"extention":"ras", "type":"image/x-cmu-raster", "reader": "readAsDataURL"},
			{"extention":"cmx", "type":"image/x-cmx", "reader": "readAsDataURL"},
			{"extention":"ico", "type":"image/x-icon", "reader": "readAsDataURL"},
			{"extention":"pnm", "type":"image/x-portable-anymap", "reader": "readAsDataURL"},
			{"extention":"pbm", "type":"image/x-portable-bitmap", "reader": "readAsDataURL"},
			{"extention":"pgm", "type":"image/x-portable-graymap", "reader": "readAsDataURL"},
			{"extention":"ppm", "type":"image/x-portable-pixmap", "reader": "readAsDataURL"},
			{"extention":"rgb", "type":"image/x-rgb", "reader": "readAsDataURL"},
			{"extention":"xbm", "type":"image/x-xbitmap", "reader": "readAsDataURL"},
			{"extention":"xpm", "type":"image/x-xpixmap", "reader": "readAsDataURL"},
			{"extention":"xwd", "type":"image/x-xwindowdump", "reader": "readAsDataURL"},
			{"extention":"mht", "type":"message/rfc822", "reader": ""},
			{"extention":"mhtml", "type":"message/rfc822", "reader": ""},
			{"extention":"nws", "type":"message/rfc822", "reader": ""},
			{"extention":"css", "type":"text/css", "reader": ""},
			{"extention":"323", "type":"text/h323", "reader": ""},
			{"extention":"htm", "type":"text/html", "reader": "readAsText"},
			{"extention":"html", "type":"text/html", "reader": "readAsText"},
			{"extention":"stm", "type":"text/html", "reader": "readAsText"},
			{"extention":"uls", "type":"text/iuls", "reader": ""},
			{"extention":"bas", "type":"text/plain", "reader": "readAsText"},
			{"extention":"c", "type":"text/plain", "reader": "readAsText"},
			{"extention":"h", "type":"text/plain", "reader": "readAsText"},
			{"extention":"txt", "type":"text/plain", "reader": "readAsText"},
			{"extention":"rtx", "type":"text/richtext", "reader": ""},
			{"extention":"sct", "type":"text/scriptlet", "reader": ""},
			{"extention":"tsv", "type":"text/tab-separated-values", "reader": ""},
			{"extention":"htt", "type":"text/webviewhtml", "reader": ""},
			{"extention":"htc", "type":"text/x-component", "reader": ""},
			{"extention":"etx", "type":"text/x-setext", "reader": ""},
			{"extention":"vcf", "type":"text/x-vcard", "reader": ""},
			{"extention":"mp2", "type":"video/mpeg", "reader": ""},
			{"extention":"mpa", "type":"video/mpeg", "reader": ""},
			{"extention":"mpe", "type":"video/mpeg", "reader": ""},
			{"extention":"mpeg", "type":"video/mpeg", "reader": ""},
			{"extention":"mpg", "type":"video/mpeg", "reader": ""},
			{"extention":"mpv2", "type":"video/mpeg", "reader": ""},
			{"extention":"mov", "type":"video/quicktime", "reader": ""},
			{"extention":"qt", "type":"video/quicktime", "reader": ""},
			{"extention":"lsf", "type":"video/x-la-asf", "reader": ""},
			{"extention":"lsx", "type":"video/x-la-asf", "reader": ""},
			{"extention":"asf", "type":"video/x-ms-asf", "reader": ""},
			{"extention":"asr", "type":"video/x-ms-asf", "reader": ""},
			{"extention":"asx", "type":"video/x-ms-asf", "reader": ""},
			{"extention":"avi", "type":"video/x-msvideo", "reader": ""},
			{"extention":"movie", "type":"video/x-sgi-movie", "reader": ""},
			{"extention":"flr", "type":"x-world/x-vrml", "reader": ""},
			{"extention":"vrml", "type":"x-world/x-vrml", "reader": ""},
			{"extention":"wrl", "type":"x-world/x-vrml", "reader": ""},
			{"extention":"wrz", "type":"x-world/x-vrml", "reader": ""},
			{"extention":"xaf", "type":"x-world/x-vrml", "reader": ""},
			{"extention":"xof", "type":"x-world/x-vrml", "reader": ""}
		],
		_fileExtention: [],
		_fileType: [],
		_fileReader: [],
		init: function(defaults) {
			var self = this;
			if (defaults && typeof defaults === "string") {
				this.defaults.type = defualts;
			} else {
				this.defaults = $.extend({}, this.defaults, defaults);
			}
			this.defaults.type = this.defaults.type.toUpperCase();
			window.requestFileSystem = window.webkitRequestFileSystem || window.requestFileSystem;
			var indexType = this._storageType.indexOf(this.defaults.type);
			if (indexType !== -1) {
				this.defaults.type = window[this._storageType[indexType]];
				this._storage = navigator[this._requestStorageType[indexType]];
			};
			for (var i = 0; i < this._fileInfo.length; i++) {
				this._fileExtention.push(this._fileInfo[i].extention);
				this._fileType.push(this._fileInfo[i].type);
				this._fileReader.push(this._fileInfo[i].reader);
			};
			try {
				if (!window.requestFileSystem) {
					throw "Error, Not Support File System!";
				}
			} catch(e) {
				alert(e.message);
			}
			return this;
		},
		request: function(success, error) {
			var self = this;
			this._storage.requestQuota(this.defaults.size, function(size) {
				window.requestFileSystem(self.defaults.type, size, function(DirectoryEntry) {
					if (success && typeof success === "function") {
						success.call(self, DirectoryEntry);
					};
				}, error);
			});
		},
		openFolder: function(path, param, success, error) {
			this.request(function(DOMFileSystem) {
				DOMFileSystem.root.getDirectory(path, param, function(DirectoryEntry) {
					if (success && typeof success === "function") {
						success.call(DOMFileSystem, DirectoryEntry);
					};
				}, error);
			}, error);
		},
		readFolder: function(path, success, error) {
			this.openFolder(path, {}, function(DirectoryEntry) {
				var reader = DirectoryEntry.createReader();
				reader.readEntries(function(Entries) {
					if (success && typeof success === "function") {
						success.call(DirectoryEntry, Entries);
					};
				});
			}, error);
		},
		newFolder: function(path, success, error) {
			path = path.split('/');
			var self = this;
			this.request(function(DOMFileSystem) {
				self._newFolder(DOMFileSystem.root, path, success, error);
			},error);
		},
		_newFolder: function(root, path, success, error) {
			var self = this;
			root.getDirectory(path[0], {create:true}, function(DirectoryEntry) {
				if (path.length) {
					if (success && typeof success === "function") {
						success.call(root, DirectoryEntry);
					};
					self._newFolder(DirectoryEntry, path.slice(1), success, error);
				};
			}, error);
		},
		renameFolder: function(path, rename, success, error) {
			this.openFolder(path, {}, function(DirectoryEntry) {
				DirectoryEntry.moveTo(this.root, rename);
				if (success && typeof success === "function") {
					success.call(this, DirectoryEntry);
				};
			}, error);
		},
		cutFolder: function(path, newPath, success, error) {
			var self = this;
			this.openFolder(path, {}, function(DirectoryEntry) {
				self.openFolder(newPath, {}, function(newDirectoryEntry) {
					DirectoryEntry.moveTo(newDirectoryEntry);
					if (success && typeof success === "function") {
						success.call(DirectoryEntry, newDirectoryEntry);
					};
				}, error);
			}, error);
		},
		copyFolder: function(path, newPath, success, error) {
			var self = this;
			this.openFolder(path, {}, function(DirectoryEntry) {
				self.openFolder(newPath, {}, function(newDirectoryEntry) {
					DirectoryEntry.copyTo(newDirectoryEntry);
					if (success && typeof success === "function") {
						success.call(DirectoryEntry, newDirectoryEntry);
					};
				}, error);
			}, error);
		},
		deleteFolder: function(path, success, error) {
			this.openFolder(path, {}, function(DirectoryEntry) {
				DirectoryEntry.removeRecursively(function() {});
				if (success && typeof success === "function") {
					success.call(this, DirectoryEntry);
				};
			}, error);
		},
		openFile: function(path, param, success, error) {
			this.request(function(DOMFileSystem) {
				DOMFileSystem.root.getFile(path, param, function(FileEntry) {
					if (success && typeof success === "function") {
						success.call(DOMFileSystem, FileEntry);
					};
				}, error);
			}, error);
		},
		readFile: function(path, success, error) {
			var file = this.infoFile(path);
			this.openFile(path, {create:false}, function(FileEntry) {
				FileEntry.file(function(File) {
					var reader = new FileReader();
					reader[file.reader](File);
					if (success && typeof success === "function") {
						success.call(FileEntry, reader);
					};
				});
			},error);
		},
		writeFile: function(path, content, success, error) {
			var file = this.infoFile(path);
			this.openFile(path, {create:false}, function(fileEntry) {
				fileEntry.createWriter(function(fileWriter) {
					fileWriter.seek(fileWriter.length);
					var blob = new Blob(content, {type:file.type});
					fileWriter.write(blob);
					if (success && typeof success === "function") {
						success.call(fileEntry, fileWriter);
					};
				}, error);
			} ,error)
		},
		infoFile: function(path) {
			var file = path.match(/\.([0-9a-z]+)(?:[\?#]|$)/i),
				key = this._fileExtention.indexOf(file[1]);
			return {
				input: path,
				name: file[0],
				extention: file[1],
				index: key,
				type: this._fileType[key],
				reader: this._fileReader[key]
			};
		},
		newFile: function(path, success, error) {
			this.openFile(path, {create:true}, success, error);
		},
		renameFile: function(path, rename, success, error) {
			this.openFile(path, {}, function(FileEntry) {
				FileEntry.moveTo(this.root, rename);
				if (success && typeof success === "function") {
					success.call(this, FileEntry);
				};
			}, error);
		},
		cutFile: function(path, newPath, success, error) {
			var self = this;
			this.openFile(path, {}, function(FileEntry) {
				self.openFolder(newPath, {}, function(newDirectoryEntry) {
					FileEntry.moveTo(newDirectoryEntry);
					if (success && typeof success === "function") {
						success.call(FileEntry, newDirectoryEntry);
					};
				}, error);
			}, error);
		},
		copyFile: function(path, newPath, success, error) {
			var self = this;
			this.openFile(path, {}, function(FileEntry) {
				self.openFolder(newPath, {}, function(newDirectoryEntry) {
					FileEntry.copyTo(newDirectoryEntry);
					if (success && typeof success === "function") {
						success.call(FileEntry, newDirectoryEntry);
					};
				}, error);
			}, error);
		},
		deleteFile: function(path, success, error) {
			this.openFile(path, {}, function(FileEntry) {
				FileEntry.remove(function() {});
				if (success && typeof success === "function") {
					success.call(this, FileEntry);
				};
			}, error);
		},
	};
	return Object.create(filesystem).init(defaults);
}
// var fs = new FileSystem();
// fs.openFile("index.html", {}, fsSuccess, fsEroor);
// fs.newFile("index.html", fsSuccess, fsEroor);
// fs.writeFile("index.html", ['Lorem ipsum dolor sit amet.'], fsSuccess, fsEroor);
// fs.readFile("index.html", fsSuccess, fsEroor);
// fs.renameFile("index.html","home.html", fsSuccess, fsEroor);
// fs.cutFile("home.html","/q", fsSuccess, fsEroor);
// fs.copyFile("q/home.html","../", fsSuccess, fsEroor);
// fs.deleteFile("q/home.html", fsSuccess, fsEroor);
// fs.request(fsSuccess, fsEroor);
// fs.openFolder("", {}, fsSuccess, fsEroor);
// fs.readFolder("", fsSuccess, fsEroor);
// fs.newFolder("q/w/e/r/t/y", fsSuccess, fsEroor);
// fs.renameFolder("q", "z", fsSuccess, fsEroor);
// fs.cutFolder("a", "apis/", fsSuccess, fsEroor);
// fs.copyFolder("apis", "css3", fsSuccess, fsEroor);
// fs.deleteFolder("apis/", fsSuccess, fsEroor);
// function fsSuccess(a) {
// 	console.log(this, a);
// }
// function fsEroor(a) {
// 	console.log(this, a);
// }
var LocalStorage = function(defaults) {
	var localstorage = {
		defaults: {
			key: undefined
		},
		init: function(defaults) {
			if (defaults && typeof defaults === "string") {
				this.defaults.key = defaults;
			} else {
				this.defaults = $.extend({}, this.defaults, defaults);
			}
			try {
				if (!window.localStorage) {
					throw "Error, Not Support Local Storage";
				} else {
					this._val();
				}
			} catch(e) {
				alert(e.message);
			}
			return this;
		},
		_set: function(value) {
			var key = this.defaults.key;
			window.localStorage.setItem(key, value);
			return false;
		},
		_get: function() {
			var key = this.defaults.key;
			return window.localStorage.getItem(key);
		},
		_val: function() {
			if (this._get() == null) {
				this._set('[]');
			};
			return eval(this._get());
		},
		add: function (data) {
			var val = this._val();
			val.push(data);
			this._set(JSON.stringify(val));
			return false;
		},
		set: function (identifier, data) {
			var val = this._val();
			var key = Object.keys(identifier);
			var index = Object.keys(data);
			var result = [];
			for (var i = 0; i < val.length; i++) {
				if (val[i][key] == identifier[key]) {
					val[i][index] = data[index];
				};
				result.push(val[i]);
			};
			this._set('[]');
			this._set(JSON.stringify(result));
			return false;
		},
		del: function (identifier) {
			var val = this._val();
			var result = [];
			if (identifier) {
				var key = Object.keys(identifier);
				for (var i = 0; i < val.length; i++) {
					if (val[i][key] !== identifier[key]) {
						result.push(val[i]);
					};
				};
			};
			this._set('[]');
			this._set(JSON.stringify(result));
			return false;
		},
		get: function (identifier) {
			var val = this._val();
			var result = identifier !== undefined ? [] : val;
			if (identifier) {
				var key = Object.keys(identifier);
				for (var i = 0; i < val.length; i++) {
					if (val[i][key] == identifier[key]) {
						result.push(val[i]);
					};
				};
			};
			return result;
		}
	};
	return Object.create(localstorage).init(defaults);
}

// var ls = LocalStorage('key');
// ls.add({
// 	id: 4,
// 	name: 'ndiing'
// });
// ls.set({
// 	id: 1
// }, {
// 	name: 'ndiing pamungkas'
// });
// ls.del();
// ls.del({
// 	id: 1
// });
// var data = ls.get();
// var data = ls.get({
// 	id: 1
// });
// console.log(data);
// var SessionStorage = function(defaults) {
// 	var sessionstorage = {
// 		defaults: {},
// 		init: function(defaults) {
// 			this.defaults = $.extend({}, this.defaults, defaults);
// 		}
// 	};
// 	return Object.create(sessionstorage).init(defaults);
// }
// var s = new SessionStorage();
// var WebNoSQL = function(defaults) {
// 	var webnosql = {
// 		defaults: {},
// 		init: function(defaults) {
// 			this.defaults = $.extend({}, this.defaults, defaults);
// 		}
// 	};
// 	return Object.create(webnosql).init(defaults);
// }
// var w = new WebNoSQL();