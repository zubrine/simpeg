<script type="text/javascript">
$(function () {
	$('.chk-checked').click(function(){
		var list_chb = $('.chb-delete');
		if( list_chb.is(':checked') )	{
			list_chb.removeAttr('checked','checked');
		}else{
			list_chb.attr('checked','checked');	
		}
	});
});

function delete_selected()
{
	var list = "";
	$('input[type=checkbox]').each(function() {     
		if (this.checked && this.value != 'on' ) {

			list += this.value + '|';
		}
	});
	
	if( list == '' ) {
		alert('<?php echo $this->l('error_no_row_selected');?>');	
	}else{
		alert(list);
	}
	
	//send data to delete
	/*$.post('http://localhost/grocery_crud/index.php/examples/delete_selection', { selection: list }, function(data) {
		alert('Voila!');
	});*/
	
}
</script>
<?php 
	$column_width = (int)(80/count($columns));
	
	if(!empty($list)){
?><div class="bDiv" >
		<table cellspacing="0" cellpadding="0" border="0" id="flex1">
		<thead>
			<tr class='hDiv'>
            	<th width="1%"><input type="checkbox" class="chk-checked" style="margin:0px;" /></th>
				<?php foreach($columns as $column){?>
				<th width='<?php echo $column_width?>%'>
					<div class="text-left field-sorting <?php if(isset($order_by[0]) &&  $column->field_name == $order_by[0]){?><?php echo $order_by[1]?><?php }?>" 
						rel='<?php echo $column->field_name?>'>
						<?php echo $column->display_as?>
					</div>
				</th>
				<?php }?>
				<?php if(!$unset_delete || !$unset_edit || !$unset_read || !empty($actions)){?>
				<th align="left" abbr="tools" axis="col1" class="" width='20%'>
					<div class="text-center">
						<?php echo $this->l('list_actions'); ?>
					</div>
				</th>
				<?php }?>
			</tr>
		</thead>		
		<tbody>
<?php foreach($list as $num_row => $row){ ?> 
<?php
		$temp_string = $row->delete_url;
		$temp_string = explode("/", $temp_string);
		$row_num = sizeof($temp_string)-1;
		$rowID = $temp_string[$row_num];
?>       
		<tr  <?php if($num_row % 2 == 1){?>class="erow"<?php }?>>
        	<td><input type="checkbox" name="delete[]" value="<?=$rowID?>" class="chb-delete" /></td>
			<?php foreach($columns as $column){?>
			<td width='<?php echo $column_width?>%' class='tdrow <?php if(isset($order_by[0]) &&  $column->field_name == $order_by[0]){?>sorted<?php }?> '>
				<div class='text-left'><?php echo $row->{$column->field_name} != '' ? $row->{$column->field_name} : '&nbsp;' ; ?></div>
			</td>
			<?php }?>
			<?php if(!$unset_delete || !$unset_edit || !$unset_read || !empty($actions)){?>
			<td align="center" width='20%'>
				<div class='tools' style="text-align:center">				
					<?php if(!$unset_delete){?>
                    	<a href='<?php echo $row->delete_url?>' title='<?php echo $this->l('list_delete')?> <?php echo $subject?>' class="delete-row" >
                    			<span class='delete-icon'></span>
                    	</a>
                    <?php }?>
                    <?php if(!$unset_edit){?>
						<a href='<?php echo $row->edit_url?>' title='<?php echo $this->l('list_edit')?> <?php echo $subject?>' class="edit_button"><span class='edit-icon'></span></a>
					<?php }?>
					<?php if(!$unset_read){?>
						<a href='<?php echo $row->read_url?>' title='<?php echo $this->l('list_view')?> <?php echo $subject?>' class="edit_button"><span class='read-icon'></span></a>
					<?php }?>
					<?php 
					if(!empty($row->action_urls)){
						foreach($row->action_urls as $action_unique_id => $action_url){ 
							$action = $actions[$action_unique_id];
					?>
							<a href="<?php echo $action_url; ?>" class="<?php echo $action->css_class; ?> crud-action" title="<?php echo $action->label?>"><?php 
								if(!empty($action->image_url))
								{
									?><img src="<?php echo $action->image_url; ?>" alt="<?php echo $action->label?>" /><?php 	
								}
							?></a>		
					<?php }
					}
					?>					
                    <div class='clear'></div>
				</div>
			</td>
			<?php }?>
		</tr>
<?php } ?>        
		</tbody>
		</table>
	</div>
<?php }else{?>
	<br/>
	&nbsp;&nbsp;&nbsp;&nbsp; <?php echo $this->l('list_no_items'); ?>
	<br/>
	<br/>
<?php }?>	
