<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends Admin_Controller
{
	public function __construct()
	{
		parent::__construct();	
	}
	
	public function index()
	{
		$data['title'] = "Main page";
		$this->load->view($this->layout,$data);
	}
	
	public function access()
	{
		echo '    [{
			"id": 1,
			"text": "KEPEGAWAIAN",
			"state": "closed",
			"children": [{
			"id": 11,
			"text": "DATA PEGAWAI",
			"url":"'.site_url("pegawai/data").'"
			},{
			"id": 12,
			"text": "Node 12",
			"url":"url_action"
			}]
			},{
			"id": 2,
			"text": "Node 2",
			"state": "closed",
			"url":"url_action"
			}]';
	}
	
	public function array_menu( $parent_id = 0 )
	{
		$arr = array();
		$query = "SELECT * FROM sys_menu WHERE is_admin = 0 AND parent_id = $parent_id AND active = 1";
		$result = $this->dbQueryCache($query,"all");
		if( count($result) > 0 )
		{
			$item = array();
			foreach((array)$result as $row)
			{
				$item['id'] = $row['id'];
				$item['parent_id'] = $row['parent_id'];
				$item['name'] = $row['name'];
				$item['url'] = $row['url'];
				$item['child'] = $this->array_menu($row['id']);
				$arr[] = $item;
			}
		}
	}
	
	
	function x()
	{
		$ok = json_decode('    [{
			"id": 1,
			"text": "KEPEGAWAIAN",
			"state": "closed",
			"children": [{
			"id": 11,
			"text": "DATA PEGAWAI",
			"url":"'.site_url("pegawai/data").'"
			},{
			"id": 12,
			"text": "Node 12",
			"url":"url_action"
			}]
			},{
			"id": 2,
			"text": "Node 2",
			"state": "closed",
			"url":"url_action"
			}]',1);
		adodb_pr($ok);
	}
}