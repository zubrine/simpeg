<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends Public_Controller
{
	public function __construct(){
		parent::__construct();	
		$this->load->library('session');
		$this->load->library('DX_Auth');
	}
	
	public function index()
	{
		echo "Halaman Login";	
	}
	
	public function login()
	{		
		
		if( isset($_POST['log_in']) )
		{
			$val = $this->form_validation;  

            $val->set_rules('username', 'Username', 'trim|required|xss_clean');  
            $val->set_rules('password', 'Password', 'trim|required|xss_clean');  
            $val->set_rules('remember', 'Remember me', 'integer');
			
			if( $val->run() == false )
			{
				$msg = validation_errors("<font color=\"red\">","</font><br>");
				set_msg( $msg );
				redirect("admin");
				exit();	
			}else{			
			
				if( $this->dx_auth->is_banned() )
				{
					$msg = error("Username Anda telah dibanned. Silahkan kontak administrator");
					set_msg( $msg );
					redirect("admin");
					exit();	
				}
				else
				{
					$username = $this->input->post('username',true);
					$password = $this->input->post('password',true);
					$remember = $this->input->post('remember');
					
					$login = $this->dx_auth->login($username, $password, $remember);
					if( $login )
					{
						redirect('cpanel/index');
						exit();	
					}
					else
					{
						$msg = "<font color=\"red\">Username atau password Anda tidak benar</font><br>";
						set_msg( $msg );
						redirect("admin");
						exit();	
					}
				}
			}
			  	
		}	
	}
	
	public function logout()
	{
		if( $this->dx_auth->is_logged_in() ){
			$this->dx_auth->logout();	
		}
		redirect('admin');
		exit();
	}
}