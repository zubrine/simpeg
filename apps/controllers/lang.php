<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Lang extends Public_Controller
{
	function __construct(){
		parent::__construct();	
	}
	
	function index()
	{
		if( !$this->uri->segment(2) ){
			echo show_404('page');
		}
		
		$this->load->library('user_agent');
		$lang = $this->uri->segment(2);
		$this->system->set_lang( strtolower($lang) );
		
		
		if ($this->agent->is_referral())
		{
			redirect( $this->agent->referrer() );
		}else{
			redirect( _URL );
		}

		exit();
	}
}