<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends Admin_Controller
{
	function __construct(){
		parent::__construct();	
	}
	
	function index()
	{
		$this->system->add_breadcrumb('Welcome');
		
		$data['page'] = 'layout_home';
		$this->load->view($this->layout,$data);
	}
	
	function dashboard()
	{
		$this->system->add_breadcrumb('Welcome');

		$group_id = $this->user->group_id;
		$sql = "SELECT m.id,parent_id,title,url,icon FROM sys_group_menu g
				LEFT JOIN sys_menu m ON(g.menu_id = m.id)
				LEFT JOIN sys_menu_text l ON(g.menu_id=l.sys_menu_id)
				WHERE is_admin = 1 AND active = 1 AND g.group_id = $group_id AND parent_id != 0 AND in_home = 1 AND lang_id = 1 GROUP BY id 
				ORDER BY parent_id, order_by, title, id ASC";
		$result = $this->adodb->GetAll($sql);
		
		$data['menu'] = $result;
		$data['page'] = 'layout_home';
		$this->load->view($this->layout_content,$data);
		
	}
	
	function menu()
	{
		$out  = "";
		$out .= "<link rel=\"stylesheet\" type=\"text/css\" href=\"{$this->theme_url}plugins/dtree/dtree.css\" />\n";
		$out .= "<div class=\"dtree\" id=\"menu_tree\"></div>\n";
		$out .= "<script type=\"text/javascript\">\n";
		$out .= "function dtree_menu(){\n";
		$out .= "obj = document.getElementById('menu_tree');\n";
		$out .= "d = new dTree('d');";
		$out .= "d.add(0,-1,'');\n";
		#$out .= "d.add(1,0,'Beranda' , '".site_url('admin/dashboard')."');\n";
		
		$base_url = base_url();
		$group_id = $this->user->group_id;
		$sql = "SELECT id,parent_id,title,url FROM sys_menu AS m 
				LEFT JOIN sys_menu_text AS t ON (m.id=t.sys_menu_id) 
				WHERE is_admin = 1 AND active = 1 AND parent_id = 0 AND lang_id = 1 AND is_cpanel = 0
				ORDER BY parent_id, order_by, title, id ASC";
	
		$result = $this->adodb->GetAll($sql);
		$menu_admin = '';
		$i = 0;
		foreach((array)$result as $row)
		{
			#$checkParent = check_user($row['url'],FALSE);
			#if( $checkParent == TRUE )
			#{
				$out .= "d.add(".$row['id'].",".$row['parent_id'].",'".$row['title']."' , '');\n";
				$sqlsub = "SELECT id,parent_id,title,url FROM sys_menu AS m 
						   LEFT JOIN sys_menu_text AS t ON (m.id=t.sys_menu_id) 
						   WHERE parent_id = ".$row['id']." AND is_admin = 1 AND is_cpanel = 0 AND active = 1 AND lang_id = 1 
						   ORDER BY parent_id, order_by, title, id ASC";
						   
				
				
				$resultsub = $this->adodb->GetAll($sqlsub);
				if( count($resultsub) > 0 )
				{
					foreach((array)$resultsub as $rowsub)
					{
						#$checkSub = check_user($rowsub['url'],FALSE);
						#if( $checkSub == TRUE )
						#{
							$out .= "d.add(".$rowsub['id'].",".$rowsub['parent_id'].",'".$rowsub['title']."','".$base_url.$rowsub['url']."','".$rowsub['title']."','mainPanel_iframe');\n";
						#}
					}
				}
			#}
		}
		
		$out .= "obj.innerHTML = d;";
		$out .= "}</script>";
		
		echo $out;
	}
	
	function user()
	{			
		#$this->adodb->debug=1;
		$this->pea->setTable("tes_table");
		$this->pea->initAjax(true);		
		$this->pea->initAdd();
		$this->pea->add->setLanguage();
	
		
		#$this->pea->add->addInput("username","text");
		#$this->pea->add->input->username->setTitle("Username");
		
		$this->pea->add->addInput("name","text");
		$this->pea->add->input->name->setTitle("name");
		$this->pea->add->input->name->setRequired(true);
		$this->pea->add->input->name->setLanguage();
		
		$this->pea->add->addInput("dateadd","date");
		$this->pea->add->input->dateadd->setTitle("Tanggal");
		$this->pea->add->input->dateadd->setFormat("y-m-d");
		$this->pea->add->input->dateadd->setTime(false);
		
		$this->pea->add->addInput("description","textarea");
		$this->pea->add->input->description->setTitle("description");
		$this->pea->add->input->description->setRequired(true);
		$this->pea->add->input->description->setLanguage();
		
		$this->pea->add->addInput("image","file");
		$this->pea->add->input->image->setTitle("Image");
		$this->pea->add->input->image->setFolder("userfiles/",base_url()."userfiles/");

		$this->pea->add->addInput("publish","checkbox");
		$this->pea->add->input->publish->setTitle("Publish");
		$this->pea->add->input->publish->setCaption("Publish");

		$form = $this->pea->add->getForm();
		
		$data['form'] = $form;
		$data['page'] = 'layout_user';
		$this->load->view($this->layout,$data);

		#adodb_pr($this->pea->add->input);
	}
	
	function content()
	{
		$this->load->view("../../themes/admin/layout_content.php");
	}
	
	function logout(){
		if( $this->dx_auth->is_logged_in() ){
			$this->dx_auth->logout();	
		}
		redirect('auth/index');
		exit();
	}
}