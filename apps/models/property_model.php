<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Property_model extends CI_Model
{
	var $query;
	var $category_id;
	var $lang_id;
	var $image_path;
	var $image_url;
	var $agent_id;
	var $navigation;

	function __construct()
	{
		parent::__construct();
		$this->lang_id = lang_id();  
		$this->image_path = _ROOT . "files/modules/property/";
		$this->image_url = _URL . "files/modules/property/";
		include_once (APPPATH . "libraries/pea/public-nav.inc.php");
	}
	
	function category_id( $id ){
		
		if( !is_numeric($id) ){
			$query = "SELECT property_category_id FROM property_category_text 
					  WHERE seo = '$id' AND lang_id = {$this->lang_id}";	
			$cat_id = $this->adodb->GetOne($query);
			$id = (int)$cat_id;
		}
		
		$this->category_id = (int)$id;
	}
	
	function agent_id($id = 0 ){
		$this->agent_id = (int)$id;	
	}
	
	function get_relation_property_id()
	{
		if( !$this->category_id ){
			return FALSE;	
		}
		
		$arr = array();
		$query = "SELECT property_id FROM property_category_relation WHERE category_id = {$this->category_id}";
		$result = $this->adodb->GetAll($query);
		foreach((array)$result as $row){
			$arr[] = $row['property_id'];
		}
		
		return $arr;
	}
	
	function get_property($perpage = 12,$is_featured = FALSE)
	{
		$clause = "";
		if( $this->category_id > 0 ){ 
			$property_id = $this->get_relation_property_id();
			
			if( count($property_id) <= 0 ){
				return FALSE;	
			}
			
			$pid = @implode(",",$property_id);
			$clause = " AND p.id IN ($pid) ";
		}
		
		if( $this->agent_id > 0 ){
			$clause = " AND user_id = {$this->agent_id} ";	
		}
		
		if( $is_featured ){
			$clause = " AND is_featured = 1 ";	
		}
		
		$query = "SELECT p.id, p.category_id, p.type_id, p.user_id, p.price, p.land_area,p.build_area,
						 p.bedrooms, p.bathrooms,p.address, p.state_id, p.city_id, p.mainimage,p.latitude,
						 p.longitude,p.is_hot, l.name,p.phoneline,p.electricity,p.rent_price,l.description,
						 p.facility,l.seo,DATE_FORMAT(p.dateadd,'%d %b %Y') AS dateadd						 
				  FROM 
				  		 property p LEFT JOIN property_text l ON(p.id=l.property_id)
				  WHERE p.publish = 1 AND l.lang_id = {$this->lang_id} $clause 
				  ORDER BY id DESC";
				  
		$nav = new oNav($query,"id",$perpage);
		
		$data = array();
		while($row = $nav->fetch()){
			$data[] = $this->parse_data($row);
		}
		
		$this->navigation = $nav->getNav();
		return (count($data) > 0) ? $data : FALSE;
		
	}
	
	function get_detail_property($id)
	{
		$id = (int)$id;
		$query = "SELECT p.id, p.category_id, p.type_id, p.user_id, p.price, p.land_area,p.build_area,
						 p.bedrooms, p.bathrooms,p.address, p.state_id, p.city_id, p.mainimage,p.latitude,
						 p.longitude,p.is_hot, l.name,p.phoneline,p.electricity,p.rent_price,l.description,
						 p.facility,l.seo,DATE_FORMAT(p.dateadd,'%d %b %Y') AS dateadd						 
				  FROM 
				  		 property p LEFT JOIN property_text l ON(p.id=l.property_id)
				  WHERE p.publish = 1 AND l.lang_id = {$this->lang_id} AND id = $id LIMIT 1";
		$result = $this->adodb->GetRow($query);
		if( count($result) <= 0 ){
			return FALSE;	
		}
		
		$result = $this->parse_data($result);
		return $result;
	}
	
	function get_property_image($id){
		return $this->adodb->GetAll("SELECT * FROM property_image WHERE property_id = $id");	
	}
	
	function get_facility_relation($id){
		$all = $this->adodb->GetAll("SELECT * FROM property_facility_relation WHERE property_id = $id");
		$arr = array();
		foreach((array)$all as $row){
			$arr[] = $row['facility_id'];
		}
		return $arr;
	}
	
	function get_category_relation($id){
		$all = $this->adodb->GetAll("SELECT * FROM property_category_relation WHERE property_id = $id");
		$arr = array();
		foreach((array)$all as $row){
			$arr[] = $row['category_id'];
		}
		return $arr;
	}
	
	function parse_data($result)
	{
		if( empty($result) ){
			return FALSE;	
		}
		
		$category = $this->get_category();
		$type = $this->get_type();
		$agent = $this->get_agent();
		$state = $this->get_state();
		$city = $this->get_city();
		$facility = $this->get_facility();
		$image = $this->get_property_image($result['id']);
		$facility_relation = $this->get_facility_relation($result['id']);
		$category_relation = $this->get_category_relation($result['id']);
		
		$arr_category = array();
		foreach((array)$category as $c_id=>$c_name){
			$c_arr = array();
			if( in_array($c_id,$category_relation) )
			{
				$arr_category[$c_id] = $c_name;
			}
		}
		
		
		
		$arrfacility = array();
		foreach((array)$facility as $id=>$name)
		{
			$arr = array();
			$arr['id'] = $id;
			$arr['name'] = $name;
			$arr['available'] = ( in_array($id,$facility_relation) ) ? TRUE : FALSE;	
			$arrfacility[] = $arr;
		}
		
		$data = array();
		$data['id'] = $result['id'];
		$data['name'] = $result['name'];
		$data['agent'] = $agent[$result['user_id']];
		$data['description'] = $result['description'];
		$data['status']['id'] = $result['type_id'];
		$data['status']['name'] = $type[$result['type_id']];
		$data['agent'] = $agent[$result['user_id']];
		$data['price']['sell'] = (int)$result['price'];
		$data['price']['rent'] = (int)$result['rent_price'];
		$data['price']['current'] = ((int)$result['rent_price'] > 0)?(int)$result['rent_price']:(int)$result['price'];
		$data['current_price'] = ( $result['price'] > 0 ) ? $result['price'] : $result['rent_price'];
		$data['size']['build'] = $result['build_area'];
		$data['size']['land'] = $result['land_area'];
		$data['bedrooms'] = $result['bedrooms'];
		$data['bathrooms'] = $result['bathrooms'];
		$data['phoneline'] = $result['phoneline'];
		$data['electricity'] = $result['electricity'];
		$data['address'] = $result['address'];
		$data['state_id'] = $result['state_id'];
		$data['city_id'] = $result['city_id'];
		$data['mainimage'] = $result['mainimage'];
		$data['latitude'] = $result['latitude'];
		$data['longitude'] = $result['longitude'];
		$data['is_hot'] = $result['is_hot'];
		
		$seo = ( empty($result['seo']) ) ? "property/detail/".$result['id']."/".url_title($result['name']) : "property/detail/".$result['seo'];
		
		$data['url']['seo'] = $seo;
		$data['url']['uri'] = "property/detail/".$result['id']."/".url_title($result['name']);
		$data['facility'] = $arrfacility;
		$data['category'] = $arr_category;
		
		
		if( file_exists($this->image_path . $data['mainimage']) ){
			$data['mainimage_url'] = _URL . "files/modules/property/".$result['mainimage'];	
		}
		
		foreach((array)$image as $img){			
			if( !empty($img['image']) and file_exists($this->image_path.$img['image']) ){				
				$data['image'][] = $this->image_url.$img['image'];
			}				
		}
		
		

		
		return $data;	
	}
	
	function get_all_category()
	{
		$data = array();
		$query = "SELECT c.id, l.name,l.seo FROM property_category c LEFT JOIN property_category_text l 
				  ON(c.id=l.property_category_id) WHERE c.publish = 1 AND l.lang_id = {$this->lang_id}";
		$result = $this->adodb->GetAll($query);
		
		return $result;
	}
	function get_category()
	{
		$data = array();
		$query = "SELECT c.id, l.name,l.seo FROM property_category c LEFT JOIN property_category_text l 
				  ON(c.id=l.property_category_id) WHERE c.publish = 1 AND l.lang_id = {$this->lang_id}";
		$result = $this->adodb->GetAll($query);
		
		foreach((array)$result as $row){
			$data[$row['id']] = $row['name'];	
		}
		return $data;
	}
	
	function get_slideshow()
	{
		$data = array();
		$query = "SELECT * FROM slideshow WHERE publish = 1 ORDER BY id";
		$result = $this->adodb->Execute($query);
		if( $result->RecordCount() > 0 )
		{
			while($row = $result->FetchRow())
			{
				
				$data[$row['id']] = $row;
				if( !empty($row['image']) and file_exists(_ROOT . "files/slideshow/".$row['image']) ){
					$data[$row['id']]['image_url'] = _URL . "files/slideshow/".$row['image'];	
				}
			}
		}
		return $data;
	}
	
	function get_agent( $perpage = "" )
	{
		$data = array();
		
		$limit = "";
		if( !empty($perpage) ){
			$limit = " LIMIT $perpage";
		}
		
		$query = "SELECT id,`name`,email,address,office_phone,phone,image,description FROM sys_users 
				  WHERE role_id = 3 ORDER BY id DESC $limit";
	
		$result = $this->adodb->GetAll($query);
		
		foreach((array)$result as $row){
			$total = $this->adodb->GetOne("SELECT COUNT(*) AS total FROM property WHERE user_id = ".$row['id']." AND publish = 1");
			
			$r = array();
			$r['id'] = $row['id'];
			$r['name'] = $row['name'];
			$r['email'] = $row['email'];
			$r['address'] = $row['address'];
			$r['phone'] = $row['phone'];
			$r['office_phone'] = $row['office_phone'];
			$r['image'] = $row['image'];
			$r['description'] = $row['description'];
			$r['total'] = $total;
			$r['image_url'] = "";
			
			if( !empty($row['image']) and file_exists(_ROOT."files/modules/agent/".$row['image']) ){
				$r['image_url'] = _URL . "files/modules/agent/".$row['image'];	
			}else{
				$r['image_url'] = $this->system->theme_url . "assets/img/agency-small-tmp.png";	
			}
			
			$data[$row['id']] = $r;	
		}
		
		return $data;
	}
	
	function get_random_agent( $perpage = "" )
	{
		$data = array();
		
		$limit = "";
		if( !empty($perpage) ){
			$limit = " LIMIT $perpage";
		}
		
		$query = "SELECT id,`name`,email,address,office_phone,phone,image,description FROM sys_users 
				  WHERE role_id = 3 ORDER BY RAND() $limit";
	
		$result = $this->adodb->GetAll($query);
		
		foreach((array)$result as $row){
			$total = $this->adodb->GetOne("SELECT COUNT(*) AS total FROM property WHERE user_id = ".$row['id']." AND publish = 1");
			
			$r = array();
			$r['id'] = $row['id'];
			$r['name'] = $row['name'];
			$r['email'] = $row['email'];
			$r['address'] = $row['address'];
			$r['phone'] = $row['phone'];
			$r['office_phone'] = $row['office_phone'];
			$r['image'] = $row['image'];
			$r['description'] = $row['description'];
			$r['total'] = $total;
			$r['image_url'] = "";
			
			if( !empty($row['image']) and file_exists(_ROOT."files/modules/agent/".$row['image']) ){
				$r['image_url'] = _URL . "files/modules/agent/".$row['image'];	
			}else{
				$r['image_url'] = $this->system->theme_url . "assets/img/agency-small-tmp.png";	
			}
			
			$data[$row['id']] = $r;	
		}
		
		return $data;
	}
	
	function get_city( $state_id = 'all' )
	{
		$data = array();
		$clause = ($state_id == 'all') ? "" : " WHERE state_id = "	 . (int)$state_id;
		$query = "SELECT * FROM city $clause ORDER BY `name` ASC";
		$result = $this->adodb->GetAll($query);
		
		foreach((array)$result as $row){
			$data[$row['id']] = $row['name'];	
		}
		
		return $data;
	}
	
	function get_state()
	{
		$data = array();
		$query = "SELECT * FROM state ORDER BY `name` ASC";
		$result = $this->adodb->GetAll($query);	
		
		foreach((array)$result as $row){
			$data[$row['id']] = $row['name'];	
		}
		
		return $data;
	}
	
	function get_type()
	{
		$data = array();
		$query = "SELECT * FROM property_type_text WHERE  lang_id = {$this->lang_id}";
		$result = $this->adodb->GetAll($query);
		
		foreach((array)$result as $row){
			$data[$row['property_type_id']] = $row['name'];	
		}
		return $data;
	}
	
	function get_facility()
	{
		$data = array();
		$query = "SELECT * FROM property_facility_text WHERE lang_id = {$this->lang_id} ORDER BY `name` ASC";
		$result = $this->adodb->GetAll($query);	
		
		foreach((array)$result as $row){
			$data[$row['property_facility_id']] = $row['name'];	
		}
		
		return $data;
	}
	
	function get_navigation(){
		return $this->navigation;	
	}
	
	function category_name_seo( $seo )
	{
		$query = "SELECT `name` FROM property_category_text 
					  WHERE seo = '$seo' AND lang_id = {$this->lang_id}";	
		$name = $this->adodb->GetOne($query);
		return $name;
	}
	
	function get_agent_dropdown()
	{
		$agent = $this->get_agent();
		$r = array();
		foreach((array)$agent as $ag){
			$r[$ag['id']] = $ag['name'];	
		}
		return $r;
	}
	
	function seo_get_id($seo)
	{
		if( empty($seo) ){
			return 0;	
		}
		
		$seo = trim($seo);
		$query = "SELECT property_id FROM property_text WHERE lang_id = {$this->lang_id} AND seo = '$seo'";
		return $this->adodb->GetOne($query);
	}
	
	function save_agent_message($post){
		if( empty($post) ){
			return FALSE;	
		}
		$post['datetime'] = date('Y-m-d H:i:s', time());
		$save = $this->adodb->AutoExecute("property_agent_msg",$post,"INSERT");
		return $save;
	}
	
	
	
	function get_related_property($cat_id)
	{
		if( is_array($cat_id) ){
			$in = implode(",",$cat_id);
			$cat = $this->adodb->GetAll("SELECT property_id FROM property_category_relation WHERE category_id IN($in)");
		}else{
			$cat = $this->adodb->GetAll("SELECT property_id FROM property_category_relation WHERE category_id IN($cat_id)"); 
		}
		$r_property_id = array();
		foreach((array)$cat as $row){
			$r_property_id[] = $row['property_id'];
		}
		
		$implode = @implode(",",$r_property_id);
		
		$query = "SELECT p.id, p.category_id, p.type_id, p.user_id, p.price, p.wide, p.bedrooms, p.bathrooms,
					 p.address, p.state_id, p.city_id, p.mainimage,p.latitude, p.longitude,p.is_hot, l.name,
					 p.phoneline,p.electricity,p.rent_price,l.description, p.facility,l.seo, 
					 DATE_FORMAT(p.dateadd,'%d %b %Y') AS dateadd						 
			  FROM 
					 property p LEFT JOIN property_text l ON(p.id=l.property_id)
			  WHERE p.publish = 1 AND l.lang_id = {$this->lang_id} AND p.id IN($implode)
			  ORDER BY id DESC";
		$result = $this->adodb->Execute($query);
		
		if( $result->RecordCount() <= 0 ){
			return FALSE;	
		}else{
			$r_row = array();
			while($data = $result->FetchRow())
			{
				$r_row[] = $this->parse_data($data);
			}
			return $r_row;
		}
	}
	
	function get_content_seo( $seo )
	{
		$id = $this->adodb->GetOne("SELECT sys_content_id FROM sys_content_text WHERE seo = '$seo'");
		return $this->get_content($id);
	}
	
	function get_content($id)
	{
		if( !$id ){
			return FALSE;	
		}
		
		$query = "SELECT c.*,l.* FROM sys_content c LEFT JOIN sys_content_text l ON(c.id=l.sys_content_id) 
				  WHERE c.id = $id AND l.lang_id = {$this->lang_id} LIMIT 1";
		$result = $this->adodb->GetRow($query);
		if( count($result) <=0 ){
			return FALSE;	
		}
		
		return $result;
	}
	
	function get_all_content($type = 1)
	{
		$query = "SELECT c.*,l.* FROM sys_content c LEFT JOIN sys_content_text l ON(c.id=l.sys_content_id) 
				  WHERE c.type_id = $type AND l.lang_id = {$this->lang_id} ORDER BY c.id DESC";
		$result = $this->adodb->GetAll($query);
		if( count($result) <= 0 ){
			return FALSE;	
		}
		return $result;
	}
	
	
	function get_marketing_of_month()
	{
		$query = "SELECT * FROM sys_users WHERE agent_month = 1 LIMIT 1";
		return $this->adodb->GetRow($query);	
	}
	
	function get_project($perpage = 10)
	{
		$query = "SELECT c.id,c.created_by_alias,c.image,DATE_FORMAT(c.created,'%d/%m/%Y') as datetime,l.* 
				  FROM sys_content c LEFT JOIN sys_content_text l ON(c.id=l.sys_content_id) 
				  WHERE c.type_id = 3 AND c.publish = 1 AND l.lang_id = {$this->lang_id} ORDER BY c.id DESC";	
		
		$nav = new oNav($query,"id",$perpage);
		
		$data = array();
		while($row = $nav->fetch()){
			$data[] = $row;
		}
		
		$this->navigation = $nav->getNav();
		return (count($data) > 0) ? $data : FALSE;
	}
	
	function get_news($perpage = 10)
	{
		$query = "SELECT c.id,c.created_by_alias,c.image,DATE_FORMAT(c.created,'%d/%m/%Y') as datetime,l.* 
				  FROM sys_content c LEFT JOIN sys_content_text l ON(c.id=l.sys_content_id) 
				  WHERE c.type_id = 2 AND c.publish = 1 AND l.lang_id = {$this->lang_id} ORDER BY c.id DESC";	
		
		$nav = new oNav($query,"id",$perpage);
		
		$data = array();
		while($row = $nav->fetch()){
			$data[] = $row;
		}
		
		$this->navigation = $nav->getNav();
		return (count($data) > 0) ? $data : FALSE;
	}
	
	
	function get_detail_news($seo)
	{
		$query = "SELECT c.id,c.created_by_alias,c.image,DATE_FORMAT(c.created,'%d/%m/%Y') as datetime,l.* 
				  FROM sys_content c LEFT JOIN sys_content_text l ON(c.id=l.sys_content_id) 
				  WHERE l.seo = '$seo' AND c.type_id = 2 AND c.publish = 1 AND l.lang_id = {$this->lang_id} 
				  ORDER BY c.id DESC";	
		$result = $this->adodb->GetRow($query);
		if( count($result) <= 0 ){
			return FALSE;	
		}
		return $result;	
	}
	
	function get_currency()
	{
		return $this->adodb->GetAll("SELECT * FROM property_currency WHERE publish = 1");		
	}
	
	function search($perpage=10)
	{
		$where = "";
		if( isset($_GET['location']) and !empty($_GET['location']) ){
			$location = $_GET['location'];
			$where = " AND p.address LIKE '%$location%' ";
		}
		
		if( isset($_GET['category']) and !empty($_GET['category']) ){			
			$category = (int)$_GET['category'];
			
			$query = "SELECT property_id FROM property_category_relation WHERE category_id = $category";
			$result = $this->adodb->GetAll($query);
			foreach((array)$result as $row){
				$arr[] = $row['property_id'];
			}
			
			if( count($arr) > 0 ){
				$property_id = 	implode(",",$arr);
				$where = " AND p.id IN ($property_id) ";
			}			
			
		}
		
		if( isset($_GET['agent']) and !empty($_GET['agent']) ){
			$agent = (int)$_GET['agent'];
			$where = " AND p.user_id = $agent ";
		}
		
		if( isset($_GET['type']) and !empty($_GET['type']) ){
			$type = (int)$_GET['type'];
			$where = " AND p.type_id = $type ";
		}
		
		if( isset($_GET['pfrom']) and !empty($_GET['pfrom']) ){			
			$curcode = ($this->session->userdata('currency'))?$this->session->userdata('currency'):'IDR';
			$pfrom = (int)$_GET['pfrom'];
			$cur = $this->get_currency_by_code( $curcode );
			$where = " AND (p.price/$cur) >= $pfrom ";
		}
		
		if( isset($_GET['pto']) and !empty($_GET['pto']) ){			
			$curcode = ($this->session->userdata('currency'))?$this->session->userdata('currency'):'IDR';
			$pfrom = (int)$_GET['pto'];
			$cur = $this->get_currency_by_code( $curcode );
			$where = " AND (p.price/$cur) <= $pto ";
		}
		
		if( isset($_GET['fland']) and !empty($_GET['fland']) ){
			$fland = (int)$_GET['fland'];
			$where = " AND p.land_area >= $fland ";
		}
		
		if( isset($_GET['tland']) and !empty($_GET['tland']) ){
			$tland = (int)$_GET['tland'];
			$where = " AND p.land_area <= $tland ";
		}
		
		if( isset($_GET['fbuild']) and !empty($_GET['fbuild']) ){
			$fbuild = (int)$_GET['fbuild'];
			$where = " AND p.build_area >= $fbuild ";
		}
		
		if( isset($_GET['tbuild']) and !empty($_GET['tbuild']) ){
			$tbuild = (int)$_GET['tbuild'];
			$where = " AND p.build_area <= $tbuild ";
		}
		
		
		$query = "SELECT p.id, p.category_id, p.type_id, p.user_id, p.price, p.land_area,p.build_area,
						 p.bedrooms, p.bathrooms,p.address, p.state_id, p.city_id, p.mainimage,p.latitude,
						 p.longitude,p.is_hot, l.name,p.phoneline,p.electricity,p.rent_price,l.description,
						 p.facility,l.seo,DATE_FORMAT(p.dateadd,'%d %b %Y') AS dateadd						 
				  FROM 
				  		 property p LEFT JOIN property_text l ON(p.id=l.property_id)
				  WHERE p.publish = 1 AND l.lang_id = {$this->lang_id} $where 
				  ORDER BY id DESC";
				  
		$nav = new oNav($query,"id",$perpage);
		
		$data = array();
		while($row = $nav->fetch()){
			$data[] = $this->parse_data($row);
		}
		
		$this->navigation = $nav->getNav();
		return (count($data) > 0) ? $data : FALSE;
		
	}
	
	function get_currency_by_code($code){
		return $this->adodb->GetOne("SELECT `value` FROM property_currency WHERE code = 'code'");	
	}
}