<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Property_model extends CI_Model
{
	var $query;
	var $category_id;
	var $lang_id;
	var $image_path;
	var $agent_id;
	
	function __construct(){
		parent::__construct();
		$this->lang_id = lang_id();  
		$this->image_path = _ROOT . "files/modules/property/";
	}
	
	function category_id( $id = 0 ){
		$this->category_id = (int)$id;
	}
	
	function agent_id($id = 0 ){
		$this->agent_id = (int)$id;	
	}
	
	function get_property( $offset = 0, $perpage = 10, $is_featured = FALSE )
	{
		
		$clause = "";
		if( $this->category_id > 0 ){ 
			$clause = " AND category_id = {$this->category_id} ";
		}
		
		if( $this->agent_id > 0 ){
			$clause = " AND user_id = {$this->agent_id} ";	
		}
		
		if( $is_featured ){
			$clause = " AND is_featured = 1 ";	
		}
		
		$query = "SELECT 
						 p.id,
						 p.category_id,
						 p.type_id,
						 p.user_id,
						 p.price,
						 p.wide,
						 p.bedrooms,
						 p.bathrooms,
						 p.address,
						 p.state_id,
						 p.city_id,
						 p.mainimage,
						 p.image1,
						 p.image2,
						 p.image3,
						 p.image4,
						 p.image5,
						 p.latitude,
						 p.longitude,
						 l.name,
						 l.description,
						 p.facility,
						 l.seo,
						 DATE_FORMAT(p.dateadd,'%d %b %Y') AS dateadd						 
				  FROM 
				  		 property p LEFT JOIN property_text l ON(p.id=l.property_id)
				  WHERE p.publish = 1 AND l.lang_id = {$this->lang_id} $clause 
				  ORDER BY 
				  		id
				  DESC LIMIT $offset,$perpage";

		$run = $this->adodb->Execute( $query );
		if( $run->RecordCount() > 0 )
		{
			$data = array();
			while( $row = $run->FetchRow() ){
				$data[$row['id']] = $this->parse_data($row);
			}
			return $data;	
			
		}else{
			return FALSE;	
		}
	}
	
	function get_category()
	{
		$data = array();
		$query = "SELECT c.id, l.name,l.seo FROM property_category c LEFT JOIN property_category_text l 
				  ON(c.id=l.property_category_id) WHERE c.publish = 1 AND l.lang_id = {$this->lang_id}";
		$result = $this->adodb->GetAll($query);
		
		foreach((array)$result as $row){
			$data[$row['id']] = $row['name'];	
		}
		return $data;
	}
	
	function get_all_category()
	{
		$data = array();
		$query = "SELECT c.id, l.name,l.seo FROM property_category c LEFT JOIN property_category_text l 
				  ON(c.id=l.property_category_id) WHERE c.publish = 1 AND l.lang_id = {$this->lang_id}";
		$result = $this->adodb->GetAll($query);
		
		return $result;
	}
	
	function get_type()
	{
		$data = array();
		$query = "SELECT * FROM property_type_text WHERE  lang_id = {$this->lang_id}";
		$result = $this->adodb->GetAll($query);
		
		foreach((array)$result as $row){
			$data[$row['property_type_id']] = $row['name'];	
		}
		return $data;
	}
	
	function get_agent()
	{
		$data = array();

		$query = "SELECT id,`name`,email,address,office_phone,phone,image,description FROM sys_users 
				  WHERE role_id = 3 ORDER BY id DESC";
	
		$result = $this->adodb->GetAll($query);
		
		foreach((array)$result as $row){
			$total = $this->adodb->GetOne("SELECT COUNT(*) AS total FROM property WHERE user_id = ".$row['id']." AND publish = 1");
			
			$r = array();
			$r['id'] = $row['id'];
			$r['name'] = $row['name'];
			$r['email'] = $row['email'];
			$r['address'] = $row['address'];
			$r['phone'] = $row['phone'];
			$r['office_phone'] = $row['office_phone'];
			$r['image'] = $row['image'];
			$r['description'] = $row['description'];
			$r['total'] = $total;
			$r['image_url'] = "";
			
			if( !empty($row['image']) and file_exists(_ROOT."files/modules/agent/".$row['image']) ){
				$r['image_url'] = _URL . "files/modules/agent/".$row['image'];	
			}else{
				$r['image_url'] = $this->system->theme_url . "assets/img/agency-small-tmp.png";	
			}
			
			$data[$row['id']] = $r;	
		}
		
		return $data;
	}
	
	function get_state()
	{
		$data = array();
		$query = "SELECT * FROM state ORDER BY `name` ASC";
		$result = $this->adodb->GetAll($query);	
		
		foreach((array)$result as $row){
			$data[$row['id']] = $row['name'];	
		}
		
		return $data;
	}
	
	function get_city( $state_id = 'all' )
	{
		$data = array();
		$clause = ($state_id == 'all') ? "" : " WHERE state_id = "	 . (int)$state_id;
		$query = "SELECT * FROM city $clause ORDER BY `name` ASC";
		$result = $this->adodb->GetAll($query);
		
		foreach((array)$result as $row){
			$data[$row['id']] = $row['name'];	
		}
		
		return $data;
	}
	
	function detail_property( $id )
	{
		
		$query = "SELECT 
						 p.id,
						 p.category_id,
						 p.type_id,
						 p.user_id,
						 p.price,
						 p.wide,
						 p.bedrooms,
						 p.bathrooms,
						 p.address,
						 p.state_id,
						 p.city_id,
						 p.mainimage,
						 p.image1,
						 p.image2,
						 p.image3,
						 p.image4,
						 p.image5,
						 p.latitude,
						 p.longitude,
						 l.name,
						 l.description,
						 l.seo,
						 p.facility,
						 DATE_FORMAT(p.dateadd,'%d %b %Y') AS dateadd						 
				  FROM 
				  		 property p LEFT JOIN property_text l ON(p.id=l.property_id)
				  WHERE p.publish = 1 AND l.lang_id = {$this->lang_id} AND p.id = $id LIMIT 1";
		$result = $this->adodb->GetRow($query);
		
		if( empty($result) ){
			return FALSE;	
		}
		
		$data = $this->parse_data($result);
		
		return $data;
		
	}
	
	function get_currency()
	{
		$data = array();
		$query = "SELECT * FROM currency WHERE publish = 1 ORDER BY id ASC";
		$result = $this->adodb->GetAll($query);
		
		foreach((array)$result as $row){
			$arr = array();
			$arr['code'] = $row['code'];
			$arr['name'] = $row['name'];
			$arr['value'] = $row['value'];
			$data[$row['id']] = $arr;
		}
		
		return $data;	
	}
	
	function parse_data($result)
	{
		if( empty($result) ){
			return FALSE;	
		}
		
		$category = $this->get_category();
		$type = $this->get_type();
		$agent = $this->get_agent();
		$state = $this->get_state();
		$city = $this->get_city();
		$facility = $this->get_facility();
		
		$data = array();
		$data['id'] = $result['id'];
		$data['name'] = $result['name'];
		$data['agent'] = $agent[$result['user_id']];
		$data['facility'] = $result['facility'];
		$data['description'] = $result['description'];
		$data['category_id'] = $result['category_id'];
		$data['category_name'] = $category[$result['category_id']];
		$data['type_id'] = $result['type_id'];
		$data['type_name'] = $type[$result['type_id']];
		$data['agent'] = $agent[$result['user_id']];
		$data['price'] = $result['price'];
		$data['wide'] = $result['wide'];
		$data['bedrooms'] = $result['bedrooms'];
		$data['bathrooms'] = $result['bathrooms'];
		$data['price'] = $result['price'];
		$data['address'] = $result['address'];
		$data['state_id'] = $result['state_id'];
		$data['state_name'] = $state[$result['state_id']];
		$data['city_id'] = $result['city_id'];
		$data['city_name'] = $city[$result['city_id']];
		$data['mainimage'] = $result['mainimage'];
		$data['latitude'] = $result['latitude'];
		$data['longitude'] = $result['longitude'];
		$data['url'] = _URL . "property/detail/$result[id]/".url_title($result['name']);
		$data['image1'] = $result['image1'];
		$data['image2'] = $result['image2'];
		$data['image3'] = $result['image3'];
		$data['image4'] = $result['image4'];
		$data['image5'] = $result['image5'];
		
		if( file_exists($this->image_path . $data['mainimage']) ){
			$data['mainimage_url'] = _URL . "files/modules/property/".$result['mainimage'];	
		}
		
		if( file_exists($this->image_path . $data['mainimage']) ){
			$data['image1_url'] = _URL . "files/modules/property/".$result['image1'];	
		}
		
		if( file_exists($this->image_path . $data['mainimage']) ){
			$data['image1_ur2'] = _URL . "files/modules/property/".$result['image2'];	
		}
		
		if( file_exists($this->image_path . $data['mainimage']) ){
			$data['image1_ur3'] = _URL . "files/modules/property/".$result['image3'];	
		}
		
		if( file_exists($this->image_path . $data['mainimage']) ){
			$data['image1_ur4'] = _URL . "files/modules/property/".$result['image4'];	
		}
		
		if( file_exists($this->image_path . $data['mainimage']) ){
			$data['image1_url5'] = _URL . "files/modules/property/".$result['image5'];	
		}
		
		$arrfacility = array();
		$propertyfacility = repairExplode($result['facility']);
		foreach((array)$facility as $id=>$name)
		{
			$arr = array();
			$arr['id'] = $id;
			$arr['name'] = $name;
			$arr['available'] = ( in_array($id,$propertyfacility) ) ? TRUE : FALSE;	
			$arrfacility[] = $arr;
		}
		
		$urlseo = ( empty($result['seo']) ) ? "" : site_url("property/detail/".$result['seo']); 
		
		$data['facility'] = $arrfacility;
		$data['seo'] = $result['seo'];
		$data['url_seo'] = $urlseo; 
		
		return $data;	
	}
	
	function get_slideshow()
	{
		$data = array();
		$query = "SELECT * FROM slideshow WHERE publish = 1 ORDER BY id";
		$result = $this->adodb->Execute($query);
		if( $result->RecordCount() > 0 )
		{
			while($row = $result->FetchRow())
			{
				
				$data[$row['id']] = $row;
				if( !empty($row['image']) and file_exists(_ROOT . "files/slideshow/".$row['image']) ){
					$data[$row['id']]['image_url'] = _URL . "files/slideshow/".$row['image'];	
				}
			}
		}
		return $data;
	}
	
	function get_facility()
	{
		$data = array();
		$query = "SELECT * FROM property_facility_text WHERE lang_id = {$this->lang_id} ORDER BY `name` ASC";
		$result = $this->adodb->GetAll($query);	
		
		foreach((array)$result as $row){
			$data[$row['property_facility_id']] = $row['name'];	
		}
		
		return $data;
	}
	
	function get_agent_by_id( $id )
	{
		$id = (int)$id;
		$query = "SELECT id,`name`,email,address,phone,office_phone,image,description FROM sys_users 
				  WHERE role_id = 3 AND id = $id ORDER BY id DESC";	
		$row = $this->adodb->GetRow($query);	
		
		$total = $this->adodb->GetOne("SELECT COUNT(*) AS total FROM property WHERE user_id = ".$row['id']." AND publish = 1");
		
		$r = array();
		$r['id'] = $row['id'];
		$r['name'] = $row['name'];
		$r['email'] = $row['email'];
		$r['address'] = $row['address'];
		$r['phone'] = $row['phone'];
		$r['office_phone'] = $row['office_phone'];
		$r['image'] = $row['image'];
		$r['description'] = $row['description'];
		$r['total'] = $total;
		
		if( !empty($row['image']) and file_exists(_ROOT."files/modules/agent/".$row['image']) ){
			$r['image_url'] = _URL . "files/modules/agent/".$row['image'];	
		}else{
			$r['image_url'] = $this->system->theme_url . "assets/img/agency-small-tmp";	
		}
		
		return $r;
	}
	
	function get_agent_dropdown()
	{
		$agent = $this->get_agent();
		$r = array();
		foreach((array)$agent as $ag){
			$r[$ag['id']] = $ag['name'];	
		}
		return $r;
	}
	
	function seo_get_id($str){
		if( empty($str) ){ return FALSE; }
		$str = trim($str);
		$query = "SELECT property_id FROM property_text WHERE seo = '$str' AND lang_id = {$this->lang_id}";
		return (int)$this->adodb->GetOne($query);
	}
	
}