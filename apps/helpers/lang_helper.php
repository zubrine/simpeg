<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function lang_assoc($id = 'id')
{
	$ci =& get_instance();
	$result = $ci->adodb->GetAll("SELECT * FROM `sys_lang` ORDER BY id ASC");
	return $result;
}


function l( $str, $return = FALSE )
{
	$ci =& get_instance();
	$oristr = trim($str);
	$str = strtolower($oristr);
	$current_lang = lang_id();	
	$data_lang = lang_assoc();
	
	$lang = $ci->adodb->GetOne("SELECT id FROM sys_lang_config WHERE name = '$str'");
	if( empty($lang) ){
		$ci->adodb->Execute("INSERT INTO sys_lang_config SET name = '$str' ");
		$config_id = $ci->adodb->Insert_ID();
		foreach((array)$data_lang as $l){
			$query = "INSERT INTO sys_lang_content SET lang_id = '".$l['id']."', config_id = '$config_id',content = '$oristr'";	
			$ci->adodb->Execute($query);
		}
	}
	
	if(isset($ci->system->lang[$str])){
		$output = $ci->system->lang[$str];
	}else{
		$output = $oristr;
	}
	return $output;	
		
}

function lang_id()
{
	$ci =& get_instance();
	return $ci->config->language['current_lang'];
}