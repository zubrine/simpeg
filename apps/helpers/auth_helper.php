<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

function encrypt_password($pass)
{
	$ci =& get_instance();
	return crypt($ci->dx_auth->_encode($pass));	
}

function check_user( $data, $redirect = true )
{
	$ci =& get_instance();
	if( empty($data) ){
		return FALSE;	
	}

	if( $ci->dx_auth->get_permission_value($data) != NULL AND $ci->dx_auth->get_permission_value($data) ){
		return TRUE;	
	}else{
		return FALSE;
		/*if( $redirect ){
			$ci->session->set_flashdata("__not_allow","You do not have permission to access this page");
			redirect('admin/dashboard');
			exit();	
		}else{
			return FALSE;	
		}*/
	}
		
}

function set_log($str)
{
	$ci =& get_instance();
	$ci->load->library('user_agent');
	
	$now 		= date('Y-m-d H:i:s');
	$thismonth 	= date('m');
	$thisyear 	= date('Y');
	$tablename 	= "sys_log_".$thismonth."_".$thisyear;
	
	$data = array();
	$data['datetime'] 	= $now;
	$data['action'] 	= strip_tags($str);
	$data['ip']			= $ci->input->ip_address();
	$data['browser']	= $ci->agent->browser();
	$data['platform']	= $ci->agent->agent_string();
	$data['url']		= $ci->uri->uri_string();	
	$data['postdata']	= isset( $_POST ) ? json_encode($_POST) : "";	
	
	$is_exists = $ci->adodb->GetOne("SHOW TABLES LIKE '$tablename'");
	if( $is_exists ){
		$ci->adodb->AutoExecute($tablename,$data,"INSERT");
	}else{
		$query = "CREATE TABLE IF NOT EXISTS `$tablename` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `datetime` datetime DEFAULT NULL,
					  `action` text,
					  `ip` varchar(255) DEFAULT NULL,
					  `browser` text,
					  `url` text,
					  `platform` text,
					  `postdata` text,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB  DEFAULT CHARSET=latin1";
		$create_table = $ci->adodb->Execute($query);
		if($create_table){
			$ci->adodb->AutoExecute($tablename,$data,"INSERT");
		}
	}
}