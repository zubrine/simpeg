<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function error( $str, $return = true ){	
	if( is_array($str) ){ $str = implode('<br>',$str);}	
	$html = html_msg($str,'error','glyphicon glyphicon-remove');	
	if( $return ){ return $html; }else{ echo $html; }	
}

function success( $str, $return = true ){	
	$html = html_msg($str,'success','glyphicon glyphicon-ok');	
	if( $return ){ return $html; }else{ echo $html;	}	
}

function info( $str, $return = true ){	
	if( is_array($str) ){ $str = implode('<br>',$str);}
	$html = html_msg($str,'info','glyphicon glyphicon-info-sign');	
	if( $return ){ return $html; }else{ echo $html;	}	
}

function html_msg( $str,$type = 'error', $icon = 'glyphicon glyphicon-ok' ){
	$icon = ($type=='error')?'glyphicon-remove':'glyphicon-ok';
	$html = "<div class=\"message-box msg-$type\">
				<strong><i class=\"$icon\"></i>".ucfirst($type)." : &nbsp;</strong>$str<br>
			</div>";
	return $html;
}

function set_msg($str){
	$ci =& get_instance();
	return $ci->session->set_flashdata('_msg',$str);
}

function get_msg(){
	$ci =& get_instance();
	if( $ci->session->flashdata('_msg') ){
		echo $ci->session->flashdata('_msg');	
	}
}