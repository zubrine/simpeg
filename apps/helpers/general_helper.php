<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

function current_theme()
{
	$obj =& get_instance();
	$theme = $obj->adodb->GetOne("SELECT `name` FROM sys_templates WHERE active = 1");
	return $theme;
}

function is_url($text)
{
	$regex	= '/^((?:ht|f)tps?:\/\/)?[a-z0-9\-]+\.[a-z0-9\-\.]+\/?/is';
	$output = (preg_match($regex, $text)) ? true : false;
	return $output;
}
// check is the string is email...
function is_email($text)
{
	$regex  = '/^[a-z0-9\_\.]+@(?:[a-z0-9\-\.]){1,66}\.(?:[a-z]){2,6}$/is';
	$output = (preg_match($regex, $text)) ? true : false;
	return $output;
}
// check is the string is phone...
function is_phone($text)
{
	$regex  = '/^\+?([0-9]+[\s\.\-]?(?:[0-9\s\.-]+)?){5,}$/is';
	$output = (preg_match($regex, $text)) ? true : false;
	return $output;
}

function config( $name = '' )
{
	if(empty($name)){ return FALSE;}
	$ci =& get_instance();
	$config = $ci->adodb->GetAssoc("SELECT `name`,`value` FROM sys_config WHERE `name` = '$name'");
	return $config[$name];
}

function tabs($data, $use_cookie = 1, $name='tabs', $maxwidth = false, $r_iframe = array(), $automodeperiod=0)
{
	global $sys;

	$out = "";
	if( count($data) > 1 )
	{
		$r_page = array();
		$i=0;
		foreach((array)$data as $pane=>$page)
		{
			$i++;
			$r_pane[] = "<li><a  rel=\"#$i\">$pane</a></li>\n";
			$r_page[] = "<div id=\"$name-page$i\" class=\"tab-content\">$page</div>\n";
		}

		if( !empty($pane) )
		{
			$out .= "<link rel=\"stylesheet\" href=\""._ASSET_URL."libraries/tabs/tabpage.css\" />\n";
			$out .= "<script type=\"text/javascript\" src=\""._ASSET_URL."libraries/tabs/tabpage.js\"></script>\n";

			$use_cookie = $use_cookie ? 'true' : 'false';
			$maxwidth = $maxwidth ? 'true' : 'false';

			$out .= "<div class=\"wrapper-tab-pane-control\">\n";
			$out .= "	<ul class=\"tab-panel\" id=\"$name\">".implode('', $r_pane)."</ul>\n";
			$out .= "	<div class=\"tab-page\" id=\"$name-page\"></div>".implode('', $r_page)."\n";
			$out .= "	<script type=\"text/javascript\">\n";
			$out .= "		var ".$name." = new danangtabs(\"".$name."\");\n";
			$out .= "		".$name.".setpersist($use_cookie);\n";
			$out .= "		".$name.".init(".$maxwidth.", ".$automodeperiod.");\n";
			$out .= "	</script>\n";
			$out .= "</div>\n";
		}
	}else{
		$out = current($data);
	}
	return $out;
}

function icons($iconname,$attr="")
{
	if( empty($iconname) ){ return false; }
	
	$arr = array();
	$attribute = "";
	
	if( is_array($attr) ){
		foreach((array)$attr as $k=>$v){
			$arr[] = $k.'="'.$v.'"';
		}
		$attribute = implode(" ",$arr);
	}else{
		$attribute = $attr;	
	}
	
	if( preg_match("#http:\/\/#is",$iconname) ){
		return "<img src=\"$iconname\" $attribute />\n";	
	}else{
		if( file_exists(_ROOT."/assets/icons/$iconname") ){
			return "<img src=\"".base_url()."assets/icons/$iconname\" $attribute />\n";
		}else{
			return "Icon ".base_url()."assets/icons/$iconname tidak ada";	
		}
	}
}

function html_menu( $arr )
{
	$out  = "";
	if( is_array($arr) and !empty($arr) )
	{
		$out .= "<ul>\n";
		foreach((array)$arr as $menu)
		{
			$out .= "<li>\n";
			$out .= "<a href=\"".$menu['url']."\">".$menu['title']."</a>\n";
			if( isset($menu['child']) and count($menu['child']) > 0 ){
				$out .= html_menu($menu['child']);
			}
			$out .= "</li>\n";
		}	
		$out .= "</ul>\n";	
	}
	
	return $out;
}

function html_menu_admin( $arr )
{
	$out  = "";
	if( is_array($arr) and !empty($arr) )
	{
		$out .= "<ul>\n";
		foreach((array)$arr as $menu)
		{
			#$allowed = check_user($menu['url'],FALSE);
			#if( $allowed )
			#{
				$icon = ( !empty($menu['icon']) ) ? "<i class=\"glyphicon ".$menu['icon']."\"></i>" : "";
				$out .= "<li>\n";	
								
				$out .= "<a href=\"#\" onclick=\"javascript:open_link('".site_url($menu['url'])."',' &raquo;&nbsp;".$menu['title']."','mainPanel_iframe');\">$icon ".$menu['title']."</a>\n";
	
				if( isset($menu['child']) and count($menu['child']) > 0 ){
					$out .= html_menu_admin($menu['child']);
				}
				$out .= "</li>\n";
			#}
		}	
		$out .= "</ul>\n";	
	}
	
	return $out;
}

function cpanel_menu( $parent_id = 0 )
{
	$ci =& get_instance();
	$sql = "SELECT id,parent_id,title,url,icon FROM sys_menu AS m 
			LEFT JOIN sys_menu_text AS t ON (m.id=t.sys_menu_id) 
			WHERE is_admin = 1 AND active = 1 AND parent_id = $parent_id AND lang_id = 1 AND is_cpanel = 1 
			ORDER BY parent_id, order_by, title, id ASC";
	$r_menu = array();
	$result = $ci->adodb->GetAll($sql);
	if(count($result)>0)
	{
		$arr = array();
		foreach((array)$result as $row)
		{
			$arr['id'] = $row['id'];
			$arr['parent'] = $row['parent_id'];
			$arr['title'] = $row['title'];
			$arr['url'] = $row['url'];
			$arr['icon'] = $row['icon'];
			$arr['child'] = cpanel_menu($row['id']);
			$r_menu[] = $arr;
		}
	}
	return $r_menu;
}

function html_permission( $arr, $groupId = 0, $deep = 0 )
{
	$ci =& get_instance();
	$out  = "";
	$permission = $ci->permissions->get_permission_data($groupId);
		
	foreach((array)$arr as $row)
	{
		if (isset($permission[$row['url']]) and $permission[$row['url']] != NULL AND $permission[$row['url']] == TRUE){
			$checked = "checked";
			$allow = 1;
		}else{
			$checked = "";
			$allow = 0;
		}
		
		$padding = ($deep*40);
		$out .= "<tr bgcolor=\"#ffffdd\" class=\"row1\">\n";
		$out .= "\t<td valign=\"top\" align=\"left\" style=\"padding-left: ".$padding."px;\">&nbsp;\n";
        $out .= "\t\t<input type=\"checkbox\" name=\"data[".$row['id']."]\" value=\"".$row['id']."\" id=\"menu".$row['id']."\" $checked class=\"click-this\" />\n";
        $out .= "\t\t<label for=\"menu".$row['id']."\">".$row['title']."</label>";
  		$out .= "\t\t<input type=\"hidden\" name=\"permission[".$row['id']."][allow]\" class=\"status-".$row['id']."\" value=\"$allow\" />\n";
        $out .= "\t\t<input type=\"hidden\" name=\"permission[".$row['id']."][name]\" value=\"".$row['title']."\" />\n";
        $out .= "\t\t<input type=\"hidden\" name=\"permission[".$row['id']."][url]\" value=\"".$row['url']."\" />\n";
        $out .= "\t\t<input type=\"hidden\" name=\"permission[".$row['id']."][id]\" value=\"".$row['id']."\" />\n";
        $out .= "\t</td>\n";
		$out .= "</tr>\n";
		
		if( !empty($row['child']) )
		{
			$out .= html_permission($row['child'],$groupId,$deep+1 );	
		}	
			
	}
	
	return $out;
}

function array_resources($parent_id = 0)
{
	$ci =& get_instance();
	$result = $ci->adodb->GetAll("SELECT * FROM sys_resources WHERE parent_id = $parent_id ORDER BY id ASC");
	
	$r_resources= array();
	if( count($result) > 0 )
	{
		$r_data = array();
		foreach((array)$result as $row)
		{
			$r_data['id'] = $row['id'];
			$r_data['parent_id'] = $row['parent_id'];
			$r_data['name'] = $row['name'];
			$r_data['child'] = array_resources($row['id']);
			$r_resources[] = $r_data;
		}
	}
	
	return $r_resources;
}

function dropdown_resources($arr, $deep = 0, $selected = "")
{
	$out  = "";
	if( !empty($arr) )
	{
		foreach((array)$arr as $r)
		{
			$check = ($r['id'] == $selected)?' selected="selected"':'';
			$sign = ( $deep == 0 )? "" : "|".str_repeat('-',($deep*2)+1);
			$out .= "<option value=\"".$r['id']."\" $check>$sign+ ".$r['name']."</option>\n";
			if( !empty($r['child']) ){
				$out .= dropdown_resources($r['child'],$deep+1, $selected);	
			}
		}
	}
	return $out;
}

function table_resources($arr, $deep = 0)
{
	$out  = "";
	if( !empty($arr) )
	{
		foreach((array)$arr as $r)
		{
			$sign = ( $deep == 0 )? "" : "|".str_repeat('-',($deep*2)+1);
			$out .= "<tr>\n";
			$out .= "\t<td width=\"85%\">$sign+ ".$r['name']."</td>\n";
			$out .= "\t<td width=\"15%\"><a href=\"".site_url('_cpanel/admin/resources/edit?id='.$r['id'])."\">".glyphicon('glyphicon-edit')." Edit</a></td>\n";
			$out .= "</tr>\n";
			if( !empty($r['child']) ){
				$out .= table_resources($r['child'],$deep+1);	
			}
		}
	}
	return $out;
}

function checkbox_resources($arr, $deep = 0)
{
	$out  = "";
	if( !empty($arr) )
	{
		foreach((array)$arr as $r)
		{
			$is_allow = check_user($r['name']);
			
			$c = ($is_allow == TRUE)?' checked="checked"':'';
			$sign = ( $deep == 0 )? "" : "|".str_repeat('-',($deep*2)+1);
			$out .= "<tr>\n";
			$out .= "\t<td width=\"5%\">
					<input type=\"checkbox\" id=\"label_".$r['id']."\" class=\"click-this\" name=\"resources[]\" value=\"".$r['id']."\" $c />
					<input type=\"hidden\" name=\"allow[".$r['id']."]\" class=\"status-".$r['id']."\" value=\"0\" />
					<input type=\"hidden\" name=\"id[]\" value=\"".$r['id']."\" />
					</td>\n";
			$out .= "\t<td width=\"80%\">$sign+ <label for=\"label_".$r['id']."\">".$r['name']."</label></td>\n";
			$out .= "\t<td width=\"15%\"><a href=\"".site_url('_cpanel/admin/resources/edit?id='.$r['id'])."\">".glyphicon('glyphicon-edit')." Edit</a></td>\n";
			$out .= "</tr>\n";
			if( !empty($r['child']) ){
				$out .= checkbox_resources($r['child'],$deep+1);	
			}
		}
	}
	return $out;
}


function glyphicon($icon=''){
	return "<i class=\"glyphicon $icon\"></i>\n";	
}

function money( $str )
{
	$separator = ".";
	$dot = ",";
	if( lang_id() == 2 ){ 
		$separator = ",";
		$dot = ".";
	}
	$format = number_format((string)$str, 2,$dot, $separator);
	return $format;
}

function wide($str){
	$format = @number_format($str, 0,"", ".");
	return $format;
}

function currency($str)
{
	return money($str);	
}

function repairExplode($data, $delimeter = ',')
{
	$arr = array();
	$r = explode($delimeter, $data);
	foreach($r AS $value)
		if(!empty($value)) $arr[] = $value;
	$output = array_unique($arr);
	return $output;
}

function repairImplode($data, $delimeter = ',')
{
	$arr = array();
	foreach((array)$data AS $value){
		if(!empty($value)) $arr[] = $value;
	}
	$output = implode($delimeter, array_unique($arr));
	if(!empty($output))
		$output = $delimeter.$output.$delimeter;
	return $output;
}

function createOption($arr, $select='')
{
	$output = '';
	$valueiskey	= $check_first = false;
	foreach((array)$arr AS $key => $dt){
		if(is_array($dt)){
			list($value, $caption) = array_values($dt);
			if(empty($caption)) $caption = $value;
		}else{
			if(!$check_first) {
				if((is_numeric($key) && $key != 0)
				|| (is_string($key) && !is_numeric($key))) {
					$valueiskey = true;
				}
				$check_first = true;
			}
			if(empty($dt) && !empty($key)) $dt = $key;
			$value = $valueiskey ? $key : $dt;
			$caption = $dt;
		}
		if(isset($select)){
			if(is_array($select)) $selected = (in_array($value, $select)) ? ' selected="selected"':'';
			else    $selected = ($value==$select) ? ' selected="selected"':'';
		}else{
			$selected = '';
		}
		$output .= "<option value=\"$value\"$selected>$caption</option>";
	}
	return $output;
}