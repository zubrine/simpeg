<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function __dropdown( $arr, $selected = 0, $returnArray = false)
{
	$out = "";
	if( !is_array($arr) ){ return $out; }
	$r = array();
	foreach((array)$arr as $row){
		$r[$row['id']] = $row['name'];
	}

	if( $returnArray ){ return $r; }
	foreach($r as $key=>$val){
		if( is_array($selected) ){
			$select = (in_array($key,$selected))?"selected":"";
		}else{
			$select = ($key==$selected)?" selected":"";
		}
		$out .= "<option value=\"$key\" $select>$val</option>\n";	
	}
	return $out;
}

function html_option( $arr, $selected = "" )
{
	$out = "";
	if( !is_array($arr) ){ return $out; }
	foreach($arr as $key=>$val){
		if( is_array($selected) ){
			$select = (in_array($key,$selected))?"selected":"";
		}else{
			$select = ($key==$selected)?" selected":"";
		}
		$out .= "<option value=\"$key\" $select>$val</option>\n";	
	}
	return $out;
}

function html_404( $status = "404", $text = "The page cannot be found" ){
	$ci =& get_instance();
	$data['page'] = $status;
	$data['status'] = $status;
	$data['text'] = $text;
	
	$ci->load->view($ci->layout,$data);
}

function gridOption($arr)
{
	$r = array();
	foreach((array)$arr as $key=>$val){
		
		if($key=='method'){
			$val = "'$val'";	
		}
		if($key=='url'){
			$val = "'$val'";	
		}
		
		$r[] = "$key:$val";
	}
	$out = implode(",",$r);
	return $out;
}


function image( $folder, $attr = "" )
{
	if( file_exists( _ROOT . $folder) ){
		$img = "<img src=\""._URL.$folder."\" $attr />\n";
		return $img;	
	}else{
		return FALSE;	
	}
}

function email_template($name)
{
	$ci =& get_instance();
	$email = $ci->adodb->GetRow("SELECT * FROM sys_email_template WHERE email_name = '$name'");
	if( count($email) > 0 ){
		return $email;	
	}
	return FALSE;
}