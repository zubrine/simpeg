<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Meta
{
	var $obj;
	var $meta_title;
	var $meta_keyword;
	var $meta_description;
	var $js_link 	= array('all'=>array(), 'meta'=>array());
	var $css_link	= array('all'=>array(), 'meta'=>array());
	var $def_css;
	var $def_js;
	
	public function __construct(){
		$this->obj =& get_instance();	
		$this->init();
	}
	
	public function init()
	{
		$this->meta_title = $this->obj->config->meta['title'];
		$this->meta_keyword = $this->obj->config->meta['keyword'];
		$this->meta_description = $this->obj->config->meta['description'];
	}
	
	function link_repair($file)
	{
		$file = str_replace(_ROOT, '', $file);
		$file = str_replace(_URL, '', $file);
		$real_file = preg_replace('~(\?.*?)?$~is', '', $file);
		
		if( is_url($file) ) { 
			return $file;
		}elseif(is_file(_ROOT.$real_file)){
			return _URL.$file;
		}else{
			return false;
		}
	}
	
	function css( $css, $is_meta = TRUE )
	{
		$css = $this->link_repair($css);
		if($css)
		{
			if(!in_array($css, $this->css_link['all']))
			{
				$this->css_link['all'][] = $css;
				if($is_meta) {
					$this->css_link['meta'][] = $css;
				}else{
					echo '<link href="'.$css.'" rel="stylesheet" type="text/css" />', "\n";
				}
			}
		}
	}
	
	function js($js, $is_meta = TRUE )
	{
		$js = $this->link_repair($js);
		if($js)
		{
			if(!in_array($js, $this->js_link['all']))
			{
				$this->js_link['all'][] = $js;
				if($is_meta) {
					$this->js_link['meta'][] = $js;
				}else{
					echo '<script src="'.$js. '" type="text/javascript"></script>', "\n";
				}
			}
		}
	}
	
	public function display( $meta = true )
	{
		$output  = "";
		$output .= "<title>".$this->meta_title."</title>\n";
		$output .= "<meta http-equiv=\"Pragma\" content=\"no-cache\">\n";
		$output .= "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n";
		
		if( $meta )
		{
		
			$output .= meta("verify-v1", session_id());
			$output .= meta("Copyright",COPYRIGHT_OWNER);
			$output .= meta("Author",_URL);
			$output .= meta("description",$this->meta_title);
			$output .= meta("keyword",$this->meta_keyword);
			$output .= meta("revisit-after","2 days");
			$output .= meta("robots","all,index,follow");		
			
			$og_title 		= $this->obj->config->meta['og']['title'];
			$og_site_name 	= $this->obj->config->meta['og']['site_name'];
			$og_url 		= $this->obj->config->meta['og']['url'];
			$og_type 		= $this->obj->config->meta['og']['type'];
			$og_description = $this->obj->config->meta['og']['description'];
			$og_image 		= $this->obj->config->meta['og']['image'];		
			
			# META OG
			$output .= "<meta property=\"og:title\" content=\"$og_title\"/>\n";
			$output .= "<meta property=\"og:site_name\" content=\"$og_site_name\"/>\n";
			$output .= "<meta property=\"og:url\" content=\"$og_url\"/>\n";
			$output .= "<meta property=\"og:type\" content=\"$og_type\"/>\n";
			$output .= "<meta property=\"og:image\" content=\"$og_image\"/>\n";
			$output .= "<meta property=\"og:description\" content=\"$og_description\"/>\n";
		
		}
		$title = $this->meta_title;
		$output .= "<link href=\""._URL."files/logo/".$this->obj->config->meta['favicon']."\" rel=\"shortcut icon\" type=\"image/x-icon\" />\n";
		$output .= "<link href=\"".site_url('content/rss')."\" rel=\"alternate\" type=\"application/rss+xml\" title=\"".$title."\" />\n";
		
		
		
		$arrcss = array_unique(array_merge(array($this->def_css), $this->css_link['meta']));
		foreach((array)$arrcss AS $css){
			if(!empty($css)){
				$output .= "<link href=\"".$css."\" rel=\"stylesheet\" type=\"text/css\" />\n";
			}
		}
		
		$arrjs = array_unique(array_merge(array($this->def_js), $this->js_link['meta']));
		foreach((array)$arrjs AS $js){
			if(!empty($js)){
				$output .= "<script src=\"".$js."\" type=\"text/javascript\"></script>\n";
			}
		}
		
		return $output;
	}
}