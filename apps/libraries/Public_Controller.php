<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Public_Controller extends MY_Controller
{
	var $is_home;
	var $theme_name;
	var $layout;
	var $theme_url;
	var $theme_path;
	var $breadcrumb_trail;
	
	var $meta_data;
	
	function __construct()
	{
		parent::__construct();
		
		$this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file'));
		
		$current_theme = current_theme();
		$this->theme_name = "easyui";
		
		$this->layout = "../../themes/{$this->theme_name}/layout_main.php";
		$this->layout_login = "../../themes/{$this->theme_name}/layout_login.php";
		$this->theme_url = _URL . "themes/{$this->theme_name}/";
		$this->theme_path = _ROOT . "themes/{$this->theme_name}/";
		
		$this->is_home = FALSE;
		
		if( !$this->uri->segment(1) ){
			$this->is_home = TRUE;	
		}
				
		if( $this->uri->segment(1) == 'home' ){
			if( !$this->uri->segment(2) or $this->uri->segment(2) == 'index' or $this->uri->segment(2) == 'main' ){
				$this->is_home = TRUE;	
			}
		}
		
		
		
		$this->config->meta['url'] = _URL;
		$this->config->meta['logo_url'] = _URL . "files/logo/".$this->config->meta['logo'];
		$this->config->template['url'] = $this->theme_url;

		$this->add_breadcrumb('Home', _URL );
		$this->load->library('Meta');
		$this->load->library('Visitor');
	}
	
	
	
	function set_lang( $code )
	{
		$lang_id = $this->db->GetOne("SELECT id FROM sys_lang WHERE code = '$code'");
		if( !empty($lang_id) ){
			$this->session->set_userdata( 'lang_id',$lang_id );	
			return TRUE;
		}
		return FALSE;
	}
	
	function dbQueryCache($query = "", $type = "", $time = 60)
	{
		$cache_name = md5( strtolower($query) );
		
	}

}