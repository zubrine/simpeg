<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class System
{
	var $ci;
	var $db;
	var $adodb;
	var $theme_url;
	var $theme_admin_url;
	var $theme_admin_path;
	var $css;
	var $js;
	var $breadcrumb_trail;
	var $js_link 	= array('all'=>array(), 'meta'=>array());
	var $css_link	= array('all'=>array(), 'meta'=>array());
	var $config;
	var $def_css;
	var $def_js;
	var $obj;
	
	function __construct(){
		$this->ci =& get_instance();
		$this->db = $this->ci->db;
		$this->adodb =& $this->ci->adodb;
		$this->set_config();
		$this->init();
		$this->get_lang();
		
	}
	
	function System()
	{
		$this->__construct();			
	}

	function admin_template(){
		return "admin";
	}
	
	function public_template(){
		return $this->config->template['theme'];	
	}
	
	function set_config()
	{
		if( !$this->config ){
			$result = $this->adodb->GetAll("SELECT * FROM sys_config");
		
			foreach((array)$result as $config){
				$conf[$config['category']][$config['name']] = $config['value'];
			}
			
			foreach($conf as $name=>$value){
				@$this->config->$name = $value;	
			}		
			
			$lang = lang_assoc();
			foreach((array)$lang as $l){
				$arr['name'] = $l['title'];
				$arr['code'] = $l['code'];
				$arr['icon'] = _URL . "files/lang/".$l['icon'];
				$this->config->lang[$l['code']] = $arr;	
			}	
		}
	}
	
	function init()
	{
		$public_theme = $this->public_template();
		$this->theme_admin_url 	= base_url() . "themes/admin/";
		$this->theme_admin_path = _ROOT . "themes/admin/";	
		
		$this->theme_path = _ROOT . "themes/$public_theme/";
		$this->theme_url = _URL . "themes/$public_theme/";
	}

	function add_css($css, $is_meta = true)
	{
		$css = $this->link_repair($css);
		if($css)
		{
			if(!in_array($css, $this->css_link['all']))
			{
				$this->css_link['all'][] = $css;
				if($is_meta) $this->css_link['meta'][] = $css;
				else echo '<link href="',$css,'" rel="stylesheet" type="text/css" />', "\n";
			}
		}
	}
	function add_js($js, $is_meta = true)
	{
		$js = $this->link_repair($js);
		if($js)
		{
			if(!in_array($js, $this->js_link['all']))
			{
				$this->js_link['all'][] = $js;
				if($is_meta) $this->js_link['meta'][] = $js;
				else echo '<script src="',$js, '" type="text/javascript"></script>', "\n";
			}
		}
	}
	function link_repair($file)
	{
		$file = str_replace(_ROOT, '', $file);
		$file = str_replace(_URL, '', $file);
		$real_file = preg_replace('~(\?.*?)?$~is', '', $file);
		if(is_url($file)) return $file;
		elseif(is_file(_ROOT.$real_file)) return _URL.$file;
		else return false;
	}
	
	function add_breadcrumb($name, $link = '')
	{
		if($name == NULL){
			return false;
		}		
		$this->breadcrumb_trail[$name] = site_url($link);
	}
	
	function breadcrumb()
	{
		$output = '';
		$i = 1;
		foreach ( $this->breadcrumb_trail as $name => $link )
		{
			if ( $i == count($this->breadcrumb_trail) )
			{
				$output .= $name;
			}
			else
			{
				$output .= anchor($link, $name);
				$output .= " &raquo; ";
			}
			$i++;
		}
		
		return $output;
	}
	
	function site_meta()
	{
		$output = '
		<title>'.$this->config->meta['title'].'</title>
		<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
		<meta name="verify-v1" content="'.session_id().'" />
		<meta name="Copyright" content="haries.web.id">
		<meta name="Author" content="'._URL.'">
		<meta name="description" content="'.$this->config->meta['description'].'">
		<meta name="keywords" content="'.$this->config->meta['keyword'].'">
		<meta name="ROBOTS" content="all, index, follow">
		<meta name="revisit-after" content="2 days">
		<link href="'._URL.'files/logo/'.$this->config->meta['favicon'].'" rel="shortcut icon" type="image/x-icon" />
		<link href="'.site_url('content/rss').'" rel="alternate" type="application/rss+xml" title="'.$this->config->meta['title'].'" />';
		$arr = array_unique(array_merge(array($this->def_css), $this->css_link['meta']));
		foreach((array)$arr AS $css){
			if(!empty($css))
			$output .= '
		<link href="'.$css.'" rel="stylesheet" type="text/css" />';
		}
		#$output .= '<script src="'.dirname($this->theme_url).'/script.js.php" type="text/javascript"><\/script>';
		$arr = array_unique(array_merge(array($this->def_js), $this->js_link['meta']));
		foreach((array)$arr AS $js){
			if(!empty($js))
			$output .= '
		<script src="'.$js.'" type="text/javascript"></script>';
		}
		$output .= "\n";
		return $output;
	}
	
	function set_lang( $code )
	{
		$lang_id = $this->ci->adodb->GetOne("SELECT id FROM sys_lang WHERE code = '$code'");
		if( !empty($lang_id) ){
			$this->ci->session->set_userdata( 'lang_id',$lang_id );	
			return TRUE;
		}
		return FALSE;
	}
	
	function get_lang()
	{
		$lang_id = $this->lang_id();
		$q = "SELECT LOWER(cfg.name) AS code, c.content
		FROM sys_lang_config AS cfg
		LEFT JOIN sys_lang_content AS c ON(cfg.id=c.config_id AND lang_id=$lang_id)";
		$r = $this->adodb->GetAssoc($q);
		foreach((array)$r AS $code => $content) {
			$this->lang[$code] = $content ? $content : $code;
		}
	}
	
	function lang_id()
	{
		
		$current_lang = $this->config->language['default_lang'];
	
		if( $this->ci->session->userdata('lang_id') ){
			$current_lang = (int)$this->ci->session->userdata('lang_id');	
		}
		return $current_lang;
	}
	
}