<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_Controller extends MY_Controller
{
	static $layout;
	static $user;
	
	function __construct(){
		parent::__construct();
		
		$libraries = array('DX_Auth','Grocery_CRUD','Meta');
		$this->load->library( $libraries );
		
		if( !$this->dx_auth->is_logged_in() ){
			redirect( base_url() );
			exit();
		}
		$this->layout 			= "../../themes/easyui/layout_main.php";
		$this->layout_content   = "../../themes/easyui/layout_content.php";
		$this->general_form		= "layout_form";
		$this->theme_url = _URL . "themes/easyui/";
		$this->theme_path = _ROOT . "themes/easyui/";
		$this->theme_name = "easyui";
		$this->__get_session();
		
		$this->add_breadcrumb('Home', 'admin/dashboard' );
		
		$this->config->meta['url'] = _URL;
		$this->config->meta['logo_url'] = _URL . "files/logo/".$this->config->meta['logo'];
	}
	
	function __get_session()
	{
		$arr = array();
		if( $this->session->userdata )
		{
			$arr['user_id'] 	= $this->session->userdata['DX_user_id'];
			$arr['username'] 	= $this->session->userdata['DX_username'];
			$arr['group_id'] 	= $this->session->userdata['DX_role_id'];
			$arr['group_name'] 	= $this->session->userdata['DX_role_name'];
			$arr['image']		= $this->adodb->GetOne("SELECT image FROM sys_users WHERE id = ".$arr['user_id']);
			$arr['name'] = $this->adodb->GetOne("SELECT `name` FROM sys_users WHERE id = ".$arr['user_id']);
			
			foreach($arr as $key=>$val){
				@$this->user->$key = $val;
			}
		}
	}
	
}