<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller
{
	var $adodb;
	var $debug = FALSE;
	var $db_var = FALSE;
	#var $_config;
	var $system;
	var $css_link;
	var $js_link;
	var $crud;
	
	function __construct(){
		parent::__construct();
		require_once(APPPATH."libraries/adodb/adodb.inc.php");	
		include_once(APPPATH."libraries/Grocery_CRUD.php");
		
		$this->load->database();
		$dsn = $this->db->dbdriver.'://'.$this->db->username.':'.$this->db->password.'@'.$this->db->hostname.'/'.$this->db->database;		
		$this->adodb =& ADONewConnection($dsn);
		
		if( $this->db_var )
		{
			$this->db =& $this->adodb;
		}
		
		if( $this->debug ){
			$this->adodb->debug = TRUE;	
		}
		#$this->system = new System;
		$this->__get_config();
		
		$this->crud = new grocery_CRUD();
	
	}
	
	function __get_config()
	{
		$result = $this->adodb->GetAll("SELECT * FROM sys_config");
		
		foreach((array)$result as $config){
			$conf[$config['category']][$config['name']] = $config['value'];
		}
		
		foreach($conf as $name=>$value){
			(object)$this->config->$name = $value;	
		}		
		
		$lang = lang_assoc();
		foreach((array)$lang as $l){
			$arr['name'] = $l['title'];
			$arr['code'] = $l['code'];
			$arr['icon'] = _URL . "files/lang/".$l['icon'];
			$this->config->lang[$l['code']] = $arr;	
		}
		
		$this->config->language['current_lang'] 	= $this->lang_id();
		$this->config->meta['og']['title'] 			= $this->config->meta['title'];
		$this->config->meta['og']['url'] 			= _URL;
		$this->config->meta['og']['site_name']		= $this->config->meta['site_name'];
		$this->config->meta['og']['image'] 			= _URL."files/logo/".$this->config->meta['logo'];
		$this->config->meta['og']['type'] 			= "article";
		$this->config->meta['og']['description'] 	= $this->config->meta['description'];
		
		
		
	}
	
	function set_config( $array = array() )
	{
		if( is_array($array) )
		{
			foreach((array)$array as $key=>$val){
				$this->config->$key = $val;	
			}
		}
	}
	
	function add_breadcrumb( $name, $link = "" )
	{
		if($name == NULL){
			return false;
		}		
		$this->breadcrumb_trail[$name] = site_url($link);
	}
	
	function breadcrumb( $separator = " &raquo; " )
	{
		$output = '';
		$i = 1;
		foreach ( $this->breadcrumb_trail as $name => $link )
		{
			if ( $i == count($this->breadcrumb_trail) )
			{
				$output .= $name;
			}
			else
			{
				$output .= anchor($link, $name);
				$output .= $separator;
			}
			$i++;
		}
		
		return $output;
	}
	
	function lang_assoc(){
		return $this->db->GetAll("SELECT * FROM `sys_lang` ORDER BY id ASC");	
	}
	
	function lang_id()
	{
		$current_lang = $this->config->language['default_lang'];
	
		if( $this->session->userdata('lang_id') ){
			$current_lang = (int)$this->session->userdata('lang_id');	
		}
		return $current_lang;
	}
	
	function set_log($str)
	{
		$this->load->library('user_agent');
		
		$now 		= date('Y-m-d H:i:s');
		$thismonth 	= date('m');
		$thisyear 	= date('Y');
		$tablename 	= "sys_log_".$thismonth."_".$thisyear;
		
		$data = array();
		$data['datetime'] 	= $now;
		$data['action'] 	= strip_tags($str);
		$data['ip']			= $this->input->ip_address();
		$data['browser']	= $this->agent->browser();
		$data['platform']	= $this->agent->agent_string();
		$data['url']		= $this->uri->uri_string();	
		$data['postdata']	= isset( $_POST ) ? $this->encrypt(json_encode($_POST)) : "";	
		
		$is_exists = $this->adodb->GetOne("SHOW TABLES LIKE '$tablename'");
		if( $is_exists ){
			$this->adodb->AutoExecute($tablename,$data,"INSERT");
		}else{
			$query = "CREATE TABLE IF NOT EXISTS `$tablename` (
						  `id` int(11) NOT NULL AUTO_INCREMENT,
						  `datetime` datetime DEFAULT NULL,
						  `action` text,
						  `ip` varchar(255) DEFAULT NULL,
						  `browser` text,
						  `url` text,
						  `platform` text,
						  `postdata` text,
						  PRIMARY KEY (`id`)
						) ENGINE=InnoDB  DEFAULT CHARSET=latin1";
			$create_table = $this->adodb->Execute($query);
			if($create_table){
				$this->adodb->AutoExecute($tablename,$data,"INSERT");
			}
		}
	}
	
	function dbQueryCache( $query = "", $type = "all", $age = 3600 )
	{
		if( function_exists('apc_add') and function_exists('apc_fetch') )
		{
			$name = 'SP_'.time().'_'.md5($query);
			$data = apc_fetch($name, $success);
			
			if (!$success) {
				$data = $this->getDataFromDB($query, $type);
				$x = apc_add($name, $data, $age);
			}
			if ($this->adodb->debug){
				if( $success ){
					echo "SQL_Cached $name: " . substr( strtolower($query), 0, 100) . '<br>';
				}
			}
			return $data;
		}
	}
	
	function getDataFromDB($sql,$type="all")
	{
		if ($type == 'all') {
			$data = $this->adodb->GetAll($sql);
		} elseif ($type == 'one') {
			$data = $this->adodb->GetOne($sql);
		} elseif ($type == 'row') {
			$data = $this->adodb->GetRow($sql);
		} elseif ($type == 'col') {
			$data = $this->adodb->GetCol($sql);
		} elseif ($type == 'assoc') {
			$data = $this->adodb->GetAssoc($sql);
		}
		return $data;	
	}
	
	function encrypt($plain_text, $password = 'OuzsPh0dq8', $iv_len = 16)
	{
	   $plain_text .= "\2008A";
	   $n = strlen($plain_text);
	   if ($n % 16) $plain_text .= str_repeat("\0", 16 - ($n % 16));
	   $i = 0;
	   $enc_text = $this->getRandomCode($iv_len);
	   $iv = substr($password ^ $enc_text, 0, 512);
	   while ($i < $n) {
		   $block = substr($plain_text, $i, 16) ^ pack('H*', md5($iv));
		   $enc_text .= $block;
		   $iv = substr($block . $iv, 0, 512) ^ $password;
		   $i += 16;
	   }
	   return base64_encode($enc_text);
	}
	
	
	function decrypt($enc_text, $password = 'OuzsPh0dq8', $iv_len = 16)
	{
	   $enc_text = base64_decode($enc_text);
	   $n = strlen($enc_text);
	   $i = $iv_len;
	   $plain_text = '';
	   $iv = substr($password ^ substr($enc_text, 0, $iv_len), 0, 512);
	   while ($i < $n) {
		   $block = substr($enc_text, $i, 16);
		   $plain_text .= $block ^ pack('H*', md5($iv));
		   $iv = substr($block . $iv, 0, 512) ^ $password;
		   $i += 16;
	   }
	   return preg_replace('/\\2008A\\x00*$/', '', $plain_text);
	}
	
	function getRandomCode($iv_len)
	{
	   $iv = '';
	   while ($iv_len-- > 0) {
		   $iv .= chr(mt_rand() & 0xff);
	   }
	   return $iv;
	}
	
}

include_once(APPPATH . "libraries/Admin_Controller.php");
include_once(APPPATH . "libraries/Public_Controller.php");